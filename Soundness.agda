open import Data.Nat

module Soundness (n : ℕ) where

open import Data.Fin hiding (_<_ ; _≤_)
open import Data.Bool renaming (_∧_ to _`and`_ ; _∨_ to _`or`_)
open import Relation.Binary.PropositionalEquality renaming (sym to _⁻¹; trans to _◾_)

open import Syntax n
open import Semantics n

soundness : ∀ {l}{Γ : Cxt l}{ψ : Props} → Γ ⊢ ψ → Γ ⊨ ψ

-----------------------------------------------------------------------------------------------------
-- Variable rules
-----------------------------------------------------------------------------------------------------

soundness {Γ = Γ ∙ ψ} var ρ _ with ⟦ Γ ⟧ᶜ ρ | ⟦ ψ ⟧ ρ
soundness {Γ = Γ ∙ ψ} var ρ _      | true   | true  = refl
soundness {Γ = Γ ∙ ψ} var ρ ()     | true   | false
soundness {Γ = Γ ∙ ψ} var ρ ()     | false  | _

soundness {Γ = Γ ∙ φ} (weaken σ) ρ _ with ⟦ Γ ⟧ᶜ ρ | inspect ⟦ Γ ⟧ᶜ ρ
soundness {Γ = Γ ∙ φ} (weaken σ) ρ _      | true   | [ ⟦Γ⟧≡true ] = soundness σ ρ ⟦Γ⟧≡true
soundness {Γ = Γ ∙ φ} (weaken σ) ρ ()     | false  | [ _ ]

-----------------------------------------------------------------------------------------------------
-- True
-----------------------------------------------------------------------------------------------------

soundness ⊤-i _ _ = refl

-----------------------------------------------------------------------------------------------------
-- False
-----------------------------------------------------------------------------------------------------

soundness (⊥-e σ) ρ ⟦Γ⟧≡true with soundness σ ρ ⟦Γ⟧≡true
soundness (⊥-e σ) ρ ⟦Γ⟧≡true    | ()

-----------------------------------------------------------------------------------------------------
-- Negation
-----------------------------------------------------------------------------------------------------

soundness {Γ = Γ} {~ ψ} (~-i σ) ρ _
  with ⟦ Γ ⟧ᶜ ρ | inspect ⟦ Γ ⟧ᶜ ρ | ⟦ ψ ⟧ ρ | inspect ⟦ ψ ⟧ ρ
...  | true     | [ ⟦Γ⟧≡true ]     | true    | [ ⟦ψ⟧≡true ]

  = soundness σ ρ (subst₂ (λ ⟦Γ⟧ ⟦ψ⟧ → ⟦Γ⟧ `and` ⟦ψ⟧ ≡ true) (⟦Γ⟧≡true ⁻¹) (⟦ψ⟧≡true ⁻¹) refl)
                          
...  | true     | [ _ ]            | false   | [ _ ] = refl

soundness {Γ = Γ} {~ ψ} (~-i σ) ρ () | false | [ _ ] | _ | [ _ ]

soundness {Γ = Γ} (~-e {ψ = ψ} σ₁ σ₂) ρ _
  with ⟦ Γ ⟧ᶜ ρ | inspect ⟦ Γ ⟧ᶜ ρ | ⟦ ψ ⟧ ρ | inspect ⟦ ψ ⟧ ρ
...  | true     | [ ⟦Γ⟧≡true ]     | true    | [ ⟦ψ⟧≡true  ]

  = (cong not ⟦ψ⟧≡true) ⁻¹ ◾ (soundness σ₂ ρ ⟦Γ⟧≡true)
  
...  | true     | [ ⟦Γ⟧≡true ]     | false   | [ ⟦ψ⟧≡false ]

  = ⟦ψ⟧≡false ⁻¹ ◾ (soundness σ₁ ρ ⟦Γ⟧≡true)
  
soundness {Γ = Γ} (~-e {ψ = ψ} σ₁ σ₂) ρ () | false  | [ _ ] | _ | [ _ ]

-----------------------------------------------------------------------------------------------------
-- Implication
-----------------------------------------------------------------------------------------------------

soundness {Γ = Γ} {φ ⇒ ψ} (⇒-i σ) ρ _
  with ⟦ Γ ⟧ᶜ ρ | inspect ⟦ Γ ⟧ᶜ ρ | ⟦ φ ⟧ ρ | inspect ⟦ φ ⟧ ρ
...  | true     | [ ⟦Γ⟧≡true ]     | true    | [ ⟦ψ⟧≡true ]

  = soundness σ ρ (subst₂ (λ ⟦Γ⟧ ⟦ψ⟧ → ⟦Γ⟧ `and` ⟦ψ⟧ ≡ true) (⟦Γ⟧≡true ⁻¹) (⟦ψ⟧≡true ⁻¹) refl)
  
...  | true     | [ _ ]            | false   | [ _ ] = refl

soundness {Γ = Γ} {φ ⇒ ψ} (⇒-i σ) ρ () | false | [ _ ] | _ | [ _ ]

soundness {ψ = ψ} (⇒-e {φ = φ} σ₁ σ₂) ρ _
  with ⟦ ψ ⟧ ρ | inspect ⟦ ψ ⟧ ρ | ⟦ φ ⟧ ρ | inspect ⟦ φ ⟧ ρ
...  | true    | [ _ ]           | _       | [ _ ] = refl

soundness {ψ = ψ} (⇒-e {φ = φ} σ₁ σ₂) ρ ⟦Γ⟧≡true | false | [ ⟦ψ⟧≡false ] | true | [ ⟦φ⟧≡true ]

  = subst₂ (λ ⟦φ⟧ ⟦ψ⟧ → not ⟦φ⟧ `or` ⟦ψ⟧ ≡ true) ⟦φ⟧≡true ⟦ψ⟧≡false (soundness σ₁ ρ ⟦Γ⟧≡true)

soundness {ψ = ψ} (⇒-e {φ = φ} σ₁ σ₂) ρ ⟦Γ⟧≡true | false | [ _ ] | false | [ ⟦φ⟧≡false ]

  = ⟦φ⟧≡false ⁻¹ ◾ soundness σ₂ ρ ⟦Γ⟧≡true

-----------------------------------------------------------------------------------------------------
-- Conjunction
-----------------------------------------------------------------------------------------------------

soundness {ψ = φ₁ ∧ φ₂} (∧-i σ₁ σ₂) ρ ⟦Γ⟧≡true
  with ⟦ φ₁ ⟧ ρ | inspect ⟦ φ₁ ⟧ ρ | ⟦ φ₂ ⟧ ρ | inspect ⟦ φ₂ ⟧ ρ
...  | true     | [ _ ]            | true     | [ _ ]          = refl
...  | true     | [ _ ]            | false    | [ ⟦φ₂⟧≡false ] = ⟦φ₂⟧≡false ⁻¹ ◾ (soundness σ₂ ρ ⟦Γ⟧≡true)
...  | false    | [ ⟦φ₁⟧≡false ]   | _        | [ _ ]          = ⟦φ₁⟧≡false ⁻¹ ◾ (soundness σ₁ ρ ⟦Γ⟧≡true)

soundness {ψ = φ} (∧-e₁ {ψ = ψ} σ) ρ ⟦Γ⟧≡true
  with ⟦ φ ⟧ ρ | inspect ⟦ φ ⟧ ρ
...  | true    | [ _ ]  = refl
...  | false   | [ ⟦φ⟧≡false ]

  = subst (λ ⟦φ⟧ → ⟦φ⟧ `and` (⟦ ψ ⟧ ρ) ≡ true) ⟦φ⟧≡false (soundness σ ρ ⟦Γ⟧≡true)

soundness {ψ = ψ} (∧-e₂ {φ = φ} σ) ρ ⟦Γ⟧≡true
  with ⟦ ψ ⟧ ρ | inspect ⟦ ψ ⟧ ρ | ⟦ φ ⟧ ρ | inspect ⟦ φ ⟧ ρ
...  | true    | [ _ ]           | _       | [ _ ] = refl
...  | false   | [ ⟦ψ⟧≡false ]   | true    | [ ⟦φ⟧≡true ]

  = subst₂ (λ ⟦φ⟧ ⟦ψ⟧ → ⟦φ⟧ `and` ⟦ψ⟧ ≡ true) ⟦φ⟧≡true ⟦ψ⟧≡false (soundness σ ρ ⟦Γ⟧≡true) 

...  | false   | [ ⟦ψ⟧≡false ]   | false   | [ ⟦φ⟧≡false ]

  = subst₂ (λ ⟦φ⟧ ⟦ψ⟧ → ⟦φ⟧ `and` ⟦ψ⟧ ≡ true) ⟦φ⟧≡false ⟦ψ⟧≡false (soundness σ ρ ⟦Γ⟧≡true)

-----------------------------------------------------------------------------------------------------
-- Disjunction
-----------------------------------------------------------------------------------------------------

soundness {ψ = φ ∨ ψ} (∨-i₁ σ) ρ ⟦Γ⟧≡true
  with ⟦ φ ⟧ ρ | inspect ⟦ φ ⟧ ρ | ⟦ ψ ⟧ ρ
... | true  | [ _ ]         | _     = refl
... | false | [ _ ]         | true  = refl
... | false | [ ⟦φ⟧≡false ] | false = ⟦φ⟧≡false ⁻¹ ◾ soundness σ ρ ⟦Γ⟧≡true

soundness {ψ = φ ∨ ψ} (∨-i₂ σ) ρ ⟦Γ⟧≡true with ⟦ φ ⟧ ρ
... | true  = refl
... | false = soundness σ ρ ⟦Γ⟧≡true

soundness {ψ = χ} (∨-e {φ = φ} {ψ = ψ} σ₁ σ₂ σ₃) ρ ⟦Γ⟧≡true
  with ⟦ χ ⟧ ρ | inspect ⟦ χ ⟧ ρ | ⟦ ψ ⟧ ρ | inspect ⟦ ψ ⟧ ρ | ⟦ φ ⟧ ρ | inspect ⟦ φ ⟧ ρ
...   | true   | [ _ ]           | _       | [ _ ]           | _       | [ _ ] = refl
...   | false  | [ ⟦χ⟧≡false ]   | true    | [ ⟦ψ⟧≡true ]    | _       | [ _ ]

  = ⟦χ⟧≡false ⁻¹ ◾ soundness σ₃ ρ (subst₂ (λ ⟦Γ⟧ ⟦ψ⟧ → ⟦Γ⟧ `and` ⟦ψ⟧ ≡ true)
                                          (⟦Γ⟧≡true ⁻¹)
                                          (⟦ψ⟧≡true ⁻¹)
                                          refl)

...   | false  | [ ⟦χ⟧≡false ]   | false   | [ ⟦ψ⟧≡false ] | true | [ ⟦φ⟧≡true ]

  = ⟦χ⟧≡false ⁻¹ ◾ soundness σ₂ ρ (subst₂ (λ ⟦Γ⟧ ⟦φ⟧ → ⟦Γ⟧ `and` ⟦φ⟧ ≡ true)
                                          (⟦Γ⟧≡true ⁻¹)
                                          (⟦φ⟧≡true ⁻¹)
                                          refl)
                                          
...   | false  | [ ⟦χ⟧≡false ]   | false   | [ ⟦ψ⟧≡false ] | false | [ ⟦φ⟧≡false ]

  =   (subst₂ (λ ⟦φ⟧ ⟦ψ⟧ → ⟦φ⟧ `or` ⟦ψ⟧ ≡ false) (⟦φ⟧≡false ⁻¹) (⟦ψ⟧≡false ⁻¹) refl) ⁻¹
    ◾ (soundness σ₁ ρ ⟦Γ⟧≡true)

-----------------------------------------------------------------------------------------------------
-- LEM
-----------------------------------------------------------------------------------------------------

soundness {ψ = φ ∨ (~ .φ)} lem ρ _ with (⟦ φ ⟧ ρ)
... | true  = refl
... | false = refl
