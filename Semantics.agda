open import Data.Nat

module Semantics (n : ℕ) where

open import Data.Fin hiding (_<_ ; _≤_)
open import Data.Bool renaming (_∧_ to _`and`_ ; _∨_ to _`or`_)
open import Relation.Binary.PropositionalEquality

open import Syntax n

-----------------------------------------------------------------------------------------------------
-- Valuation
-----------------------------------------------------------------------------------------------------

Val : Set
Val = Fin n → Bool

-----------------------------------------------------------------------------------------------------
-- Semantics
-----------------------------------------------------------------------------------------------------

⟦_⟧ : Props → Val → Bool
⟦ ⊥       ⟧ ρ = false
⟦ ⊤       ⟧ ρ = true
⟦ patom x ⟧ ρ = ρ x
⟦ ~ φ     ⟧ ρ = not (⟦ φ ⟧ ρ)
⟦ φ₁ ∨ φ₂ ⟧ ρ = ⟦ φ₁ ⟧ ρ `or`  ⟦ φ₂ ⟧ ρ
⟦ φ₁ ∧ φ₂ ⟧ ρ = ⟦ φ₁ ⟧ ρ `and` ⟦ φ₂ ⟧ ρ
⟦ φ₁ ⇒ φ₂ ⟧ ρ = not (⟦ φ₁ ⟧ ρ) `or` ⟦ φ₂ ⟧ ρ 

⟦_⟧ᶜ : {l : ℕ} → Cxt l → Val → Bool
⟦ ∅      ⟧ᶜ ρ = true
⟦ xs ∙ x ⟧ᶜ ρ = ⟦ xs ⟧ᶜ ρ `and` ⟦ x ⟧ ρ

_⊨_ : {l : ℕ} → Cxt l → Props → Set
Γ ⊨ ψ = ∀ ρ → ⟦ Γ ⟧ᶜ ρ ≡ true → ⟦ ψ ⟧ ρ ≡ true

infix 5 _⊨_
