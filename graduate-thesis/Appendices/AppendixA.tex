\chapter{Detailed Explanations of the Proof for the Soundness and Completeness}

In this appendix, some detailed explanations about how to prove the soundness and completeness are demonstrated since only a few of them are shown in the dissertation for the simplicity and readability of the dissertation.

\section{Soundness part}

When proving the soundness, we checked the form of the derivation tree $\Gamma \vdash \psi$ in order to use structural induction on it. Chapter 3 only uses the case of conjunction introduction to illustrate the idea. Here we present a more detailed analysis for each case to help readers understand the proof and the program entirely. This can be regarded as a supplement of the section 3.3.

\begin{description}
	\item [\texttt{var} rule:] To prove the soundness when the last rule is variable rule i.e. Γ ∙ ψ ⊢ ψ → Γ ∙ ψ ⊨ ψ, we only need to discuss the truth value of $\psi$. If everything in the context $\Gamma\ \cdot\ \psi$ evaluates to true, then $\psi$ must evaluate to true since it is in the context. 
	\item [\texttt{weak} rule:] If the last rule is weakening rule i.e. the soundness proof requires the proof of $\Gamma\ \cdot\ \phi \vdash \psi \rightarrow \Gamma\ \cdot\ \phi \vDash \psi$, given the condition that $\Gamma \vdash \psi$ is valid, then it can be proved by  using the induction hypothesis. Since we know $\Gamma \vdash \psi$, we can get $\Gamma \vDash \psi$ by using induction hypothesis. If all propositions in the context $\Gamma\ \cdot\ \phi$ is true, then everything in $\Gamma$ must be true. Therefore we can say that $\psi$ evaluates to true based on $\Gamma \vDash \psi$. 
	\item [$\top$-i rule:] This case is trivial since $\top$ evaluates to true everywhere. 
	\item [$\bot$-e rule:] If the last rule we use is $\bot$-elimination rule i.e. the soundness states $\Gamma \vdash \psi \rightarrow \Gamma \vDash \psi$ given $\Gamma \vdash \bot$ is valid, then this means the context is inconsistent because the only way to deduce $\bot$ is to obtain a proposition $\phi$ and its negation ¬$\phi$ at the same time. Under this circumstance, the context contains at least one false premise. Therefore we can say $\Gamma \vDash \psi$ since anything can be proven by false premises. 
	\item [\texttt{\~}-i rule:] If we suppose the last rule is \verb|~|-introduction rule i.e. $\psi$ has form \verb|~|$\psi$ and soundness says $\Gamma \vdash$\verb|~|$\psi \rightarrow \Gamma \vDash$\verb|~|$\psi$ given that Γ ∙ ψ ⊢ ⊥ is valid, then we know $\Gamma\ \cdot\ \psi \vDash \bot$ which means it is impossible that the semantics of $\Gamma$ and $\psi$ are both true because in that case the semantics of $\bot$ is true. If the semantics of $\Gamma$ is false, then from false we can derive everything. Thus $\Gamma \vDash$\verb|~|$ \psi$ holds. If the semantics of $\Gamma$ is true, then the meaning of $\psi$ must be false. Therefore the meaning of \verb|~|$\psi$ must be true. In this case, $\Gamma \vDash$\verb|~|$\psi$ holds as well.
	\item [\texttt{\~}-e rule:] When the last rule is \verb|~|-elimination rule, we need to prove Γ ⊢ ⊥ $\rightarrow \Gamma \vDash \bot$ given that Γ ⊢ ψ and Γ ⊢ \verb|~|ψ are both valid. We can get $\Gamma \vDash \psi$ and $\Gamma \vDash$\verb|~|$\psi$ by using induction hypothesis. If the meaning of $\psi$ is true, then the the meaning of \verb|~|$\psi$ is false. Thus we can use Γ $\vDash$ \verb|~|ψ to prove that from the context $\Gamma$ we can prove something that is false. Then we can say $\Gamma \vDash \bot$ holds. Similarly, when the meaning of $\psi$ is false, we can also use it to prove $\Gamma \vDash \bot$.
	\item [⇒-i:] If the last rule is ⇒-introduction, then the conclusion has the form φ ⇒ ψ. We need to prove that Γ ⊢ φ ⇒ ψ → Γ ⊨ φ ⇒ ψ given Γ ∙ φ ⊢ ψ. From the induction hypothesis, we know that Γ ∙ φ ⊨ ψ. Then we discuss the semantics of φ. If the meaning of φ is true, then if the meaning of Γ is also true, we know the meaning of ψ is true based on Γ ∙ φ ⊨ ψ. Since φ and ψ are both true when Γ is true we know the proposition φ ⇒ ψ is also true when Γ is true. Therefore Γ ⊨ φ ⇒ ψ holds. If the meaning of φ is false, then the meaning of φ ⇒ ψ is true regardless of the meaning of ψ. In this case, Γ ⊨ φ ⇒ ψ holds as well.
	\item [⇒-e:] When the last rule is ⇒-elimination, we need to prove Γ ⊨ ψ given Γ ⊢ φ ⇒ ψ and Γ ⊢ φ. First the meaning of φ should be discussed. If it is true, then we know if Γ is true, φ ⇒ ψ and φ are both true from the induction hypothesis. To guarantee this, ψ must be true and thus Γ ⊨ ψ holds. If the meaning of φ is false, then since Γ ⊨ φ holds we can say that from Γ we can derive a proposition which is false. Because of that we know Γ can semantically entails anything. So Γ ⊨ ψ holds. 
	\item [∧-i:] When the conclusion has form φ ∧ ψ, the soundness asserts that Γ ⊢ φ ∧ ψ → Γ ⊨ φ ∧ ψ given that both Γ ⊢ φ and Γ ⊢ ψ are valid. From induction hypothesis we can obtain Γ ⊨ φ and Γ ⊨ ψ. Therefore we know that when the meaning of Γ is true, φ and ψ are both true. Based on the definition of how ∧ operator deals with boolean values, we know the meaning of φ ∧ ψ is true when the meaning of Γ is true. Thus Γ ⊨ φ ∧ ψ holds. 
	\item [∧-e₁:] If the proof uses ∧-elimination₁ as the last rule, then the soundness requires us to prove Γ ⊢ φ → Γ ⊨ φ given Γ ⊢ φ ∧ ψ. This is obvious because from the conditions we have, we can Γ ⊨ φ ∧ ψ. When the meaning of Γ is true, we know the semantics of φ ∧ ψ is true. The only way to make it true is that both $\phi$ and $\psi$ are true. Therefore Γ ⊨ φ holds. 
	\item [∧-e₂:] The proof for this rule is similar to the previous one. 
	\item [∨-intro₁:] When the last rule is ∨-introduction₁, the soundness states that Γ ⊢ φ ∨ ψ → Γ ⊨ φ ∨ ψ given Γ ⊢ φ. When the meaning of Γ is true, we know φ is also true from the induction hypothesis. With respect to the meaning of φ ∨ ψ, it must be true since we already have φ be true. Therefore Γ ⊨ φ ∨ ψ holds. 
	\item [∨-i₂:] This proof is similar to the previous one. 
	\item [∨-e:] If the proof needs ∨-elimination, it has the proofs for Γ ⊢ φ ∨ ψ, Γ ∙ φ ⊢ χ and Γ ∙ ψ ⊢ χ. The soundness asserts that Γ ⊢ χ → Γ ⊨ χ. When the meaning of Γ is true, we know that the meanings of φ ∨ ψ is true from the induction hypothesis. That means at least one of φ and ψ is true. If φ is true, then we know that χ is true from Γ ∙ φ ⊨ χ. If ψ is true, then we know χ is true from Γ ∙ ψ ⊨ χ. At least one of these cases must be true. Either way Γ ⊨ χ holds. 
	\item [lem:] If the proof uses the law of the excluded middle, then the soundness says Γ ⊢ ψ ∨ \verb|~| ψ → Γ ⊨ ψ ∨ \verb|~| ψ. The meaning of ψ ∨ \verb|~| ψ is always true no matter what truth value ψ has. Therefore it is obvious that Γ ⊨ ψ ∨ \verb|~| ψ holds.
\end{description}

\section{Completeness Part}

When proving the Lemma 2.1.1 and Lemma 2.1.2 in the completeness part, we need to pattern match on $\psi$. We used the conjunction form as our example there to show the idea. Here we give all the forms of the formula $\psi$ and present the proof trees of it.

\begin{description}
    \item [Lemma 2.1.1] ∀ ψ ρ, ⟦ ψ ⟧ ρ ≡ true → Cxt[ρ] ⊢ ψ
    \item [Lemma 2.1.2] ∀ ψ ρ, ⟦ ψ ⟧ ρ ≡ false → Cxt[ρ] ⊢ \verb|~|ψ
\end{description}


\begin{description}
	\item [$\bot\ \top$:] These are easy to prove since their Cxt[ρ] only contains themselves. 
	\item [$patom\ i$:] This is obvious to us but not so to Agda. We know the $i$th propositional atom or its negation must be in Cxt[$\rho$] since this context contains all the propositional atoms or their negations by definition. This can cause some trouble in Agda but the idea is obvious. 
	\item [\texttt{\~}$\phi$:] If $\psi$ has this form, then we discuss the meaning of $\psi$. If it is true, then the meaning of $\phi$ is false. From the induction hypothesis, we know Cxt[ρ] ⊢ \verb|~| $\phi$. Therefore Cxt[ρ] ⊢ ψ is valid. When the meaning of $\psi$ is false, the proof is similar but we need to use double negation at some point. 
	\item [φ₁ ∨ φ₂:] The form of $\psi$ is a disjunction. If the meaning of $\psi$ is true, then we know at least one of φ₁ and φ₂ is true. We only need to find out the true one and use induction hypothesis to prove Cxt[ρ] ⊢ φ₁ or Cxt[ρ] ⊢ φ₂. Then we use one of $\lor$-introduction rules to prove Cxt[ρ] ⊢ φ₁ ∨ φ₂. \\ If the meaning of $\psi$ is false, then both of φ₁ and φ₂ evaluate to false. We can use induction hypothesis to prove Cxt[ρ] ⊢ \verb|~| φ₁ and Cxt[ρ] ⊢ \verb|~| φ₂. Then we can prove Cxt[ρ] ⊢ \verb|~| (φ₁ ∨ φ₂). Let $\Gamma$ be Cxt[ρ] $\cdot$ φ₁ ∨ φ₂.
		\begin{prooftree}
	    	\AxiomC{$\Gamma$ $\vdash$ φ₁ ∨ φ₂}
	    	\AxiomC{$\Gamma\ \cdot$ φ₁ $\vdash\ \bot$}
	    	\AxiomC{$\Gamma\ \cdot$ φ₂ $\vdash\ \bot$}
	    	\RightLabel{$\lor$-e}
	    	\TrinaryInfC{$\Gamma\ \vdash\ \bot$}
	    \end{prooftree}
	    Therefore we know that Cxt[ρ] $\cdot$ φ₁ ∨ φ₂ $\vdash\ \bot$. So we can prove Cxt[ρ] ⊢ \verb|~| (φ₁ ∨ φ₂) by using \verb|~|-introduction. 
	\item [φ₁ ∧ φ₂:] In the conjunction case, if the meaning of $\psi$ is true then both φ₁ ∧ φ₂ are true. By using induction hypothesis, we can easily prove Cxt[ρ] ⊢ φ₁ and Cxt[ρ] ⊢ φ₂. Applying $\land$-introduction on them can obtain Cxt[ρ] ⊢ φ₁ $\land$ φ₂. \\ If $\psi$ is assigned to false, then we look at which one of φ₁ and φ₂ is false. Let us suppose that φ₁ is false and $\Gamma$ is Cxt[$\rho$] $\cdot$ φ₁ ∧ φ₂. Then from induction hypothesis we know Cxt[ρ] ⊢ \verb|~| φ₁.
		\begin{prooftree}
			\AxiomC{φ₁ ∧ φ₂ $\in\ \Gamma$}
			\RightLabel{var}
			\UnaryInfC{$\Gamma\ \vdash$ φ₁ ∧ φ₂}
			\RightLabel{$\land$-e$_1$}
			\UnaryInfC{$\Gamma\ \vdash$ φ₁}
			\AxiomC{$\Gamma \vdash$ \texttt{\~} φ₁}
			\RightLabel{\texttt{\~}-e}
			\BinaryInfC{$\Gamma\ \vdash\ \bot$}
		\end{prooftree}
		Thus we can prove Cxt[ρ] ⊢ \verb|~| (φ₁ ∧ φ₂). Similarly, if $\phi_2$ evaluates to false, we use the same strategy to deduce $\bot$ and prove Cxt[ρ] ⊢ \verb|~| (φ₁ ∧ φ₂). 
	\item [φ₁ ⇒ φ₂:] If $\psi$ has this form, we first consider the semantics of $\phi_1$. We suppose it is false because this is an easier case. Then $\psi$ must be true, which means our goal is to prove Cxt[ρ] ⊢ φ₁ ⇒ φ₂. Since we already suppose $\phi_1$ is false, we can get Cxt[ρ] ⊢ \verb|~| φ₁ and use it to prove $\bot$. Then we can use $\bot$-elimination to prove everything. Let $\Gamma$ be Cxt[$\rho$].
		\begin{prooftree}
			\AxiomC{$\phi_1\ \in\ \Gamma\ \cdot\ \phi_1$}
			\RightLabel{var}
			\UnaryInfC{$\Gamma\ \cdot\ \phi_1\ \vdash\ \phi_1$}
			\AxiomC{$\Gamma\ \vdash\ $\texttt{\~}$ \phi_1$}
			\RightLabel{weaken}
			\UnaryInfC{$\Gamma\ \cdot\ \phi_1\ \vdash$ \texttt{\~} $\phi_1$}
			\RightLabel{\texttt{\~}-e}
			\BinaryInfC{$\Gamma\ \cdot\ \phi_1\ \vdash\ \bot$}
			\RightLabel{$\bot$-e}
			\UnaryInfC{$\Gamma\ \cdot\ \phi_1\ \vdash\ \phi_2$}
			\RightLabel{$\Rightarrow$-i}
			\UnaryInfC{$\Gamma\ \vdash\ \phi_1\ \Rightarrow\ \phi_2$}
		\end{prooftree}
		Next let us see if $\phi_1$ evaluates to true, then we need to discuss the semantics of $\phi_2$. If the meaning of $\phi_2$ is true, then we want to prove Cxt[ρ] ⊢ φ₁ ⇒ φ₂ given Cxt[ρ] ⊢ φ₂. This is easy to prove since we only need to weaken Cxt[ρ] ⊢ φ₂ to Cxt[ρ] $\cdot$ φ₁ $\vdash$ φ₂ and use $\Rightarrow$-introduction. If the meaning of $\phi_2$ is false, then we should prove Cxt[ρ] ⊢ $\sim$ (φ₁ ⇒ φ₂) given Cxt[ρ] ⊢ $\sim$ φ₂ and Cxt[ρ] ⊢ φ₁. To keep the proof tree shorter and readable, we let $\Gamma$ be Cxt[$\rho$] $\cdot$ (φ₁ ⇒ φ₂):
		\begin{prooftree}
			\AxiomC{Cxt[ρ] ⊢ φ₁}
			\RightLabel{weaken}
			\UnaryInfC{$\Gamma$ ⊢ φ₁}
			\AxiomC{φ₁ ⇒ φ₂ $\in\ \Gamma$}
			\RightLabel{var}
			\UnaryInfC{$\Gamma\ \vdash$ φ₁ ⇒ φ₂}
			\RightLabel{$\Rightarrow$-e}
			\BinaryInfC{$\Gamma$ ⊢ φ₂}
			\AxiomC{$\Gamma$ ⊢ $\sim$ φ₂}
			\RightLabel{\texttt{\~}-e}
			\BinaryInfC{$\Gamma\ \vdash\ \bot$}
		\end{prooftree}
		Therefore we know Cxt[$\rho$] $\cdot$ (φ₁ ⇒ φ₂) $\vdash\ \bot$. So we can prove Cxt[$\rho$] $\vdash\ \sim$ (φ₁ ⇒ φ₂) by using \verb|~|-introduction.
\end{description}