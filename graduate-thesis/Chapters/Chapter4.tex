\chapter{Conclusion and discussion}

In this last chapter, we give the conclusion of the project and discussion on the problems in the project. Then we propose some possible ways of improving the proof like making it shorter and more general so that the philosophy behind it can be reused for other works.

\section{Conclusion of the work}

\subsection{Summary}
This dissertation implements in Agda a formalisation of the proof for
soundness and completeness of classical propositional logic. It also presents an accessible proof of the completeness theorem to help readers to understand what role the law of the excluded middle plays in it. 

The idea of the proof follows Huth and Ryan's work \cite{CUP_book} in general but includes my personal methodologies inside especially the idea of the cut-off style context to assist with the use of the law of the excluded middle and induction. This idea is not obvious and it is done with much effort. It is the very core thing in this project, which plays a very important part and is totally my original thought. It results from the fact that in the proofs we cannot use the number of propositional atoms in a given proposition but we do need to do mathematical induction somewhere. Therefore we introduce in the length information for contexts and make their constructor be capable of accepting a new natural number $a$ specifying how many propositions we want to take out of contexts. In the end, a rigorous and clear proof of completeness theorem in classical propositional logic is made.

The point of doing this project consists of two aspects. The first one is to provide a machine-assisted mathematical proof of the completeness theorem for classical propositional logic. Reading proofs on a book can help people to get the general idea and usually people think they understand the proof. Sometimes this is not true because people will not think about the detailed computation steps inside a proof and they just get ``the idea''. However, reading a proof in a functional programming language leads people to a deeper understanding since by realising how the computer computes the functions, people can gain profound understanding about how the computation of the logical calculus is done. They will find exactly how the variables are operated and computed. It is even very educational to build a machine checked proof themselves since it forces programers to understand the proof entirely without any loophole. In our case, one should fully know how to apply the law of the excluded middle on the proof so that every arbitrary proposition can be proved in a uniform way. 

The second is to show the advantages to use Agda to construct computer-aided proofs. In this dissertation, we have seen how clever Agda is, i.e. it can automatically guess the types of implicit variables in order to reduce the length of the program. It can also support Unicode and Greek letters in its code so mathematicians can write proofs just like what they usually do on a piece of paper. The theory behind this tool is Martin-L\"{o}f's type theory \cite{HoTTbook} \cite{MartinLof:1996uy} which provides programmers with a framework full of freedom to formalise any formal logical system.

\subsection{Main difficulties}

Since this is a project relevant to theoretical topics in computer science and to the best knowledge of us no one has done it yet, this project contains many uncertainties. 

First of all, Agda is only a powerful framework and the entire logical system and its definitions must be designed by the programmer. Any inappropriate definition might lead to a dead end of the whole proof. To find the best definitions, many attempts have been done along the project process and some thoughts were brought from Keller and Altenkirch's work \cite{Keller:2010ik} and Allais' unpublished work written for his MSc internship under the supervision of Thorsten Altenkirch \cite{allais}.

Secondly, though people are familiar with the proof of the completeness of classical propositional logic, it is still a tough task to convert the idea of the proof into Agda programs. One should know how to do the propositional calculus in the proof for every step thoroughly. In the meantime, type consistency and program totality must be handled cautiously. In order
to construct the proof with Agda recognisable functions, we even have to
find some more general propositions to prove so that the functions we
need to construct in Agda are structurally recursive and can pass the
termination checker. The example is that we generalise Lemma 2.2 to Lemma 2.2.1 in order to create the condition for us to use recursion. The original proof (Lemma 2.2) then will become a special
case of the more general proposition. Most of the time, the generalised form of the lemma can accept the proof by induction whilst the original one cannot. This intention in
return provides us with a higher level view of what the proof actually
computes.

\section{Discussion on the problems and further work}

\subsection{The indexing of $Cxt[\rho]$}
One aggravating problem in this implementation is in the $Cxt[\rho](a)$. In our Agda code, the \verb|Cxt[_]| function requires two parameters: the length information \verb|a| and the proof showing that \verb|a| $\leq$ \verb|n|. Intuitively, this should be able to be replaced by finite sets in Agda so that the function only needs one parameter which has type \verb|Fin (suc n)| because it can represent both meanings above: it is a natural number which can specify the length and it satisfies the restriction that it is smaller or equal than \verb|n|. 

The main reason why I did not use \verb|Fin (suc n)| in this project is that it can cause some trouble when defining the function $Cxt[\rho]$. To illustrate this, we can simply see how to construct this function step by step in Agda in order to give readers a direct feeling of this problem. If we change the definition of the \verb|Cxt[_]| in section 3.4.2 to the one shown below:

\begin{code}
\>\AgdaFunction{Cxt[\_]} \AgdaSymbol{: ∀ (ρ :} \AgdaDatatype{Fin} \AgdaSymbol{n →} \AgdaDatatype{Bool}\AgdaSymbol{) → (a :} \AgdaDatatype{Fin} \AgdaSymbol{(suc n)) →} \AgdaDatatype{Cxt} \AgdaSymbol{(toℕ a)} \<%    
\end{code}

then one natural way of building this is to pattern match on \verb|a|: if it is \verb|zero|, then return $\varnothing$ as the context because the length of it is zero. If it is \verb|suc a|, then we recursively call \verb|Cxt[_]| to build the first \verb|a| elements and use $\hat p_i[\_]$ to build the (\verb|suc a|)th element. The program looks like:

\begin{code}
\>\AgdaFunction{Cxt[ ρ ]} \AgdaInductiveConstructor{zero} \<%
\>[1]\AgdaSymbol{=} \AgdaInductiveConstructor{∅} \<%
\\
\>\AgdaFunction{Cxt[ ρ ]} \AgdaSymbol{(}\AgdaInductiveConstructor{suc} \AgdaSymbol{a)} \<%
\>[1]\AgdaSymbol{= \{!!\}} \AgdaInductiveConstructor{∙} \AgdaSymbol{\{!!\}} \<%    
\end{code}

Ideally, the content  in the first \verb|{!!}| should be something like \verb|Cxt[ ρ ](a)| to create the first \verb|a| elements. However, we will notice a type inconsistency in this recursive call. The type of \verb|ρ| remains unchanged, but the type of \verb|a| becomes \verb|Fin n| due to the built-in implementation of finite sets in Agda library. If we look back at the type signature of \verb|Cxt[_]|, then we will see that the type of \verb|a| must be \verb|Fin (suc n)| if \verb|ρ| is still \verb|Fin n → Bool|. Therefore our \verb|a| does not have the type Agda requires. If we lift \verb|a| from \verb|Fin n| into \verb|Fin (suc n)| by using some function, e.g. an embedding function \verb|emb|, then Agda will also complain about the term \verb|(emb a)| because the right hand side does not use \verb|a| directly. This cannot satisfy the elimination rule and the reduction rule in functional programming. Thus it will not pass the termination checker.

So far I have not found a way of using \verb|Fin (suc n)| to replace the two parameters in the current implementation. The root of this problem is that the length \verb|a| is bound to the number of the propositional atoms \verb|n| if we use \verb|Fin (suc n)| as its type. We combine these two things together but in our implementation the requirement is to only change one of them (which is \verb|a|) and keep the other one (which is \verb|suc n|) unchanged. We even do not have the authority to write a piece of code to force \verb|(suc n)| not to reduce to \verb|n| because that is determined by the finite sets implemented in Agda library. The current solution in this dissertation splits up these two things and only do the recursive call on one of them so that it can not only construct the correct $Cxt[\rho](a)$ but also pass the Agda termination checker.

Although I cannot solve this problem at this point, it is still possible that there exists one way of using finite sets to represent the whole thing. One main task in the future would be to rewrite the definition of the $Cxt[\rho]$ to make the proof shorter.

\subsection{Improvement of the proof of the completeness part}

The completeness part is proved in three steps as is shown in previous chapters. Basically it only does two things: 

\begin{enumerate}
    \item Prove a weaker version of the completeness, i.e. $\varnothing \vDash \psi \rightarrow \varnothing \vdash \psi$;
    \item Extend this thing to contexts by using Lemma 1 and Lemma 3, i.e. $\Gamma \vDash \psi \rightarrow \Gamma \vdash \psi$.
\end{enumerate}

Our method is to move everything in $\Gamma$ to the right and move them back to the left after we have proved the weaker version of the completeness. This method is quite specific to this project. In other words, it might only be suitable for classical propositional logic and the logical system we built in this project may not be reusable for further work or other research topics relevant to formalising completeness theorems. 

One possible alternative way is to change the way we define the derivation tree. In this dissertation, the data type \verb|_⊢_| only allows one proposition on the right. We can extend it to contexts, i.e. we can write $\Gamma \vdash \Delta$ in which both $\Gamma$ and $\Delta$ are contexts and this means all propositions in $\Delta$ can be deduced by using deduction rules and the premises in $\Gamma$. Then we can do the step 2 above in the following way:

\centerline{\texttt{∀ (ρ: Fin n → Bool) → (Cxt[ρ] ⊢ Γ → Cxt[ρ] ⊢ ψ) → Γ ⊢ ψ}}

We also need another lemma: 

\centerline{\texttt{Cxt[ρ] ⊢ Γ → ⟦ Γ ⟧ᶜ ρ ≡ true}}

After combining them together we can get the proof of the completeness part. This method is more general since it does not contain any special operations like moving premises to the right and moving them back in the current method. We do not know whether this new implementation can work but it is a very good direction to try. The difficulty lies in the definition of this extended version of \verb|_⊢_|, which would be very interesting to be solved in the further work.

\subsection{Completeness of the predicate logic}

As is mentioned in the section 1.3 -- Related work, formalising completeness theorems of different logical systems and models are widely researched by many researchers. We have presented a unified way of constructing proofs for propositional logic in this dissertation (see the \emph{Remark} 3.2 in the section 3.4.2). One natural thought about the next step is predicate logic since it is usually the next thing to learn after students have learned propositional logic in logic courses. 

Nevertheless, computer scientists are familiar with the famous Church-Turing thesis \cite{TURING:1937ec} that the predicate logic is undecidable if we use boolean model. In other words, we cannot find a unified way to decide whether a given predicate logic formula is \emph{universally valid}. 

Therefore, we will look at predicate logic in different models instead of using classical logic, i.e. we do not use boolean model to define its semantics but use models like Kripke model and Beth model just like what is listed in the section 1.3. The next step may follow any of these research results.

\begin{remark}
Some of further work has already been done like normalisation by completeness in minimal logic and the completeness of it proved directly with contexts instead of using the ``3 steps'' style. The corresponding code has been submitted in the public repository\footnote{\url{http://bitbucket.org/Leran/propositional-logic}}. The repository was created for the Interactive Theorem Proving 2015 conference since the earlier version of this dissertation was submitted for this conference in March. More implementation about the further work can be found in the repository, but it will not be presented in this dissertation to keep the topic concentrated.
\end{remark}

%\subsection{Normalisation by completeness}
%An interesting further step would be to investigate the computational
%content of this proof in the style of normalisation by completeness
%(\cite{berger91nbe}, \cite{alti:ctcs95}). The completeness and the
%soundness proofs can be composed which gives a normalisation procedure
%on proof trees (which can be thought about as computer programs).
%
%\begin{code}
%\>\AgdaFunction{nbc} \AgdaSymbol{: ∀\{l\}\{Γ :} \AgdaDatatype{Cxt} \AgdaSymbol{l\}\{φ :} \AgdaDatatype{Props}\AgdaSymbol{\} → Γ} \AgdaDatatype{⊢} \AgdaSymbol{φ → Γ} \AgdaDatatype{⊢} \AgdaSymbol{φ} \<%
%\\
%\>\AgdaFunction{nbc} \AgdaSymbol{=} \AgdaFunction{completeness ∘ soundness} \<%
%\end{code}
