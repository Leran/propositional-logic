\chapter{Background Knowledge}

This chapter will first introduce Agda in more details. Then classical propositional logical system which the proof uses will be 
presented, including how we should write logical sentences (syntax) and which 
model we want to use for the meanings of propositions (semantics) inside the system. 
At last, the idea of the proof of the soundness and completeness will be presented.

\section{Description of the main tool -- Agda}

All proofs were conducted in Agda (\cite{norell07thesis},
\cite{agdaWiki}), an interactive proof assistant which implements
Martin-L\"{o}f type theory \cite{HoTTbook} \cite{MartinLof:1996uy} and can be used as a
framework to formalise formal logical systems. It is also a dependently
typed functional programming language with inductive families. $\Pi$
types (dependent functions) are written by the generalised arrow
notation \texttt{(x:A) → B} which can be interpreted as $\forall x \in
A.B$ by the Curry-Howard correspondence. \emph{Implicit arguments} which we introduced in the previous chapter are
denoted by curly brackets e.g. \texttt{f:\{x:A\} → B}, in this case it is
not necessary to provide these arguments as Agda will try to deduce
them. We can still provide them by the \texttt{f \{x = ...\}}
notation. The notation \texttt{∀\{x\}} abbreviates \texttt{\{x:\_\}} where
the type of \texttt{x} is inferred by Agda, the underline character
denotes a wildcard. Agda functions can be defined with infix or mixfix
notations, e.g. \verb|_⇒_| is an infix function, \verb|~_| is a prefix
operator. The underlines denote the positions of arguments.

Mathematical proofs in Agda are written as structurally recursive
functions. Therefore, constructing a mathematical proof in Agda is
equivalent to constructing a well-typed function which can
terminate. Induction is performed by pattern matching on the arguments
of the function. Inductive types, and more generally, inductive
families can be defined by using the \verb|data| keyword.

In addition, as is mentioned in the first chapter, Agda supports Unicode characters, which allows us to write proofs that look like common logic proofs on textbooks. This is important to this dissertation because one main purpose of the project is to write a computer-aided proof to teach students why classical propositional logic is sound and complete.

\section{Classical propositional logical system}

The formalisation of the completeness theorem requires a proper design
of classical propositional logical system in which the syntax and
semantics should be carefully defined. For any formal system, the syntax and semantics are the most fundamental components that constitute the system. Briefly, the syntax defines how to write formulas over the alphabet. In our case, the alphabet is \{$p_0,p_1...$\} $\cup$ \{\verb|~|, $\land$, $\lor$, $\Rightarrow$\}. The semantics states how to understand the meaning of a given formula. Without the syntax, people can not write down abstract symbols to represent the real objects in this world. Without semantics, the formulas essentially are just some meaningless strings of symbols.

\subsection{Syntax}
For the syntax in our system, a crucial question is how to define propositional formulas. A formula is only a string of meaningless symbols which can be calculated based on some predefined rules. This section will introduce two ways of building reasonable and ``legal'' formulas in our syntax so that absurd formulas like $p_0 \Rightarrow \lor\ p_1\ p_2$ will never appear in our logical system.  

\subsubsection{Well-formed fomulas}
A propositional
formula is a string of indivisible propositional atoms and logical
connectives, but the sequence of symbols must be restricted by some conditions. We can specify how to construct well-formed propositional 
formulas in our logical system.

\begin{definition}
\textbf{(Well-formed formulas)} Well-formed formulas in propositional logical system are 
formulas which can be constructed by using the following rules finite times:
\end{definition}

\begin{description}
    \item [atom:] All propositional atoms are well-formed, e.g. $p_0, p_1,...$
    \item [negation $($\texttt{\~}$)$: ] If $\phi$ is well-formed, the \verb|~| $\phi$ is also well-formed.
    \item [conjunction $(\land)$:] If $\phi$ and $\psi$ are well-formed, then $\phi \land \psi$ is also well-formed.
    \item [disjunction $(\lor)$:] If $\phi$ and $\psi$ are well-formed, then $\phi \lor \psi$ is also well-formed.
    \item [implication $(\Rightarrow)$:] If $\phi$ and $\psi$ are well-formed, then $\phi \Rightarrow \psi$ is also well-formed.
\end{description}

By using this definition, we can guarantee that every propositional formula is ``legal''. The previous example $p_0 \Rightarrow \lor\ p_1\ p_2$ cannot appear because if we regard the thing on the right hand side of $\Rightarrow$ as well-formed, i.e. $\lor\ p_1\ p_2$ can be constructed by using the rules above, then we will find the contradiction here since there must be another well-formed formula on the left of $\lor$. In this way we can justify whether a given propositional formula is well-formed. If we draw the syntax tree of a well-formed formula, then all leaves will contain propositional atoms and all internal nodes will contain logical connectives.

\begin{remark}
Previously we use propositional variables P and Q as our propositional components in a proposition in the first chapter. This way of naming propositions is abandoned in the rest of the dissertation because we only use indivisible propositional atoms to represent the basic component of propositions. The propositional variable is different from propositional atoms we defined above. In some contexts, a propositional variable can mean divisible propositions. To be safe, we use the term ``propositional atom'' to mean that the proposition can not be divided into smaller parts. 
\end{remark}


\subsubsection{Deduction rules}

In the previous section, we introduced how to write a well-formed formula by picking symbols from the alphabet and combining them together in a proper sequence. This can be seen as creating formulas from the scratch, like building blocks. In propositional logic, the deduction rules define how we infer new formulas from previous formulas. This is another way of writing well-formed formulas. This can be seen as drawing conclusions given a certain set of premises. 

To make this more accessible, we use Peano arithmetic \cite{Peano:1889vi} as an analogy. The symbols ``0'' and ``s'' actually have no meanings if we treat them as some strange symbols. However, if we define some rules like: a valid string is either ``0'' or an ``s'' followed by a valid string. Then we can write all strings like ``0, s0, ss0,...'', but the strings like ``s0s'' will never appear. Even though we only had two symbols at the beginning, we end up with infinitely many strings by just defining one rule and doing operations on them. Even more, they seem to have special meanings because strings like ``ss0, ssss0'' can easily remind people of natural numbers, which makes the strings derived look reasonable to people. In classical propositional logic, similar things happen. The process of doing propositional calculus is essentially the reasoning of the validity of the propositions and we use deduction rules to build new reasonable formulas.

An entire list of natural deduction rules are presented in Figure 2.1 and they can be used for the existent propositions to calculate more propositions. The proof in this dissertation adopts classical logic, thus \emph{the law of the excluded middle}(LEM) is included. This rule characterises classical propositional logic. A similar thing is \emph{intuitionistic logic} \cite{vanDalen:2001wg} which is often referred to when discussing classical logic. Intuitionistic logicians believe that we are able to claim something is true if and only if we have a proof of it, i.e. we know what its proof tree looks like. Thus in intuitionistic logic, the law of the excluded middle is rejected because we cannot prove $\phi$ $\lor$ \verb|~|$\phi$ from nothing. We have to at least be able to construct the proof of $\phi$ or the proof of \verb|~|$\phi$. There exist some propositions which cannot be proved in intuitionistic logic. For example, Pierce's formula $((\phi \Rightarrow \psi) \Rightarrow \phi) \Rightarrow \phi$ cannot be constructed by using deduction rules without the law of the excluded middle, but it is always true in its truth table. Due to the existence of these propositions, intuitionistic logic with boolean model is not complete. From this example, we can see how important the law of the excluded middle is in our completeness theorem.

\begin{figure}[h]
\begin{center}
	\begin{tabular}{| >{\vspace{0.3cm}\centering\arraybackslash}m{1.5cm}<{\vspace{0.3cm}} | >{\vspace{0.3cm}\centering\arraybackslash}m{5cm}<{\vspace{0.3cm}} >{\vspace{0.3cm}\centering\arraybackslash}m{5cm}<{\vspace{0.3cm}} |}
	\hline
			&	\textbf{Introduction} & \textbf{Elimination} \\
	\hline
	$\land$ &	\AxiomC{$\phi$}
				\AxiomC{$\psi$}
				\RightLabel{$\land$-i}
				\BinaryInfC{$\phi\ \land\ \psi$}
				\DisplayProof 
			&	\AxiomC{$\phi\ \land\ \psi$}
				\RightLabel{$\land$-e$_1$}
				\UnaryInfC{$\phi$}
				\DisplayProof
				\AxiomC{$\phi\ \land\ \psi$}
				\RightLabel{$\land$-e$_2$}
				\UnaryInfC{$\psi$}
				\DisplayProof \\
	$\lor$	&	\AxiomC{$\phi$}
				\RightLabel{$\lor$-i$_1$}
				\UnaryInfC{$\phi\ \lor\ \psi$}
				\DisplayProof
				\AxiomC{$\phi$}
				\RightLabel{$\lor$-i$_2$}
				\UnaryInfC{$\phi\ \lor\ \psi$}
				\DisplayProof
			&   \AxiomC{$\phi\ \lor\ \psi$}
				\AxiomC{$\phi$}
				\noLine
				\UnaryInfC{$\vdots$}
				\noLine
				\UnaryInfC{$\chi$}
				\AxiomC{$\psi$}
				\noLine
				\UnaryInfC{$\vdots$}
				\noLine
				\UnaryInfC{$\chi$}
				\RightLabel{$\lor$-e}
				\TrinaryInfC{$\chi$}
				\DisplayProof \\
	$\Rightarrow$  & \AxiomC{$\phi$}
					 \noLine
					 \UnaryInfC{$\vdots$}
					 \noLine
					 \UnaryInfC{$\psi$}
					 \RightLabel{$\Rightarrow$-i}
					 \UnaryInfC{$\phi\ \Rightarrow\ \psi$}
					 \DisplayProof
				   & \AxiomC{$\phi$}
				   	 \AxiomC{$\phi\ \Rightarrow\ \psi$}
				   	 \RightLabel{$\Rightarrow$-e}
				   	 \BinaryInfC{$\psi$}
				   	 \DisplayProof\\
	\verb|~| & 	\AxiomC{$\phi$}
				\noLine
				\UnaryInfC{$\vdots$}
				\noLine
				\UnaryInfC{$\bot$}
				\RightLabel{\texttt{\~}-i}
				\UnaryInfC{\texttt{\~}$\phi$}
				\DisplayProof 
		   &	\AxiomC{$\phi$}
		   		\AxiomC{\texttt{\~}$\phi$}
		   		\RightLabel{\texttt{\~}-e}
		   		\BinaryInfC{$\bot$}
		   		\DisplayProof \\	
	$\bot$ & (No introduction rule for $\bot$)
		   &  	\AxiomC{$\bot$}
				\RightLabel{$\bot$-e}
				\UnaryInfC{$\phi$}
				\DisplayProof \\
	\hline
	\textbf{LEM} & \multicolumn{2}{ c |}{\AxiomC{}
										 \RightLabel{LEM}
										 \UnaryInfC{$\phi\ \lor\ $\texttt{\~}$\phi$}
										 \DisplayProof} \\
	\hline
	\end{tabular}
\end{center}
\caption{Natural deduction rules for propositional logic}
\end{figure}

So far we have seen how to write well-formed formulas in our logical system. As we have promised before, we demonstrate the proof tree of the example we used in the first chapter (P $\Rightarrow$ Q) $\land$ P $\Rightarrow$ Q where $\Rightarrow$-e is just the \emph{modus ponens} rule we mentioned in that chapter (Figure 2.2).

\begin{figure}[h]
\begin{prooftree}
    \AxiomC{(P $\Rightarrow$ Q) $\land$ P}
    \RightLabel{$\land$-e$_2$}
    \UnaryInfC{P}
    \AxiomC{(P $\Rightarrow$ Q) $\land$ P}
    \RightLabel{$\land$-e$_1$}
    \UnaryInfC{P $\Rightarrow$ Q}
    \RightLabel{$\Rightarrow$-e}
    \BinaryInfC{Q}
    \RightLabel{$\land$-i}
    \UnaryInfC{(P $\Rightarrow$ Q) $\land$ P $\Rightarrow$ Q}
\end{prooftree}
\caption{The proof tree of the example in Chapter 1}
\end{figure}

In Agda, we can define the derivation tree as inductive types and
thereby finding the main connective and making the tree structure of a
formula explicit become trivial. The deduction rules will be implemented as 
constructors of the proof tree type. The detailed code will be shown in 
the next chapter.

\subsection{Semantics}

As for semantics, every proposition in classical propositional logic
can be assigned with a boolean value. This can be implemented as a
function which takes a propositional formula as its input and its
output is the meaning of the formula. 

\begin{figure}[h]
\begin{center}
	\begin{tabular}{>{\vspace{0.3cm}\centering\arraybackslash}m{3cm}<{\vspace{0.3cm}} >{\vspace{0.3cm}\centering\arraybackslash}m{3cm}<{\vspace{0.3cm}} >{\vspace{0.3cm}\centering\arraybackslash}m{3cm}<{\vspace{0.3cm}}}
	\begin{tabular}{c | c | c}
		$\phi$     & $\psi$     & $\phi\ \land\ \psi$ \\
		\hline
		\texttt{T} & \texttt{T} & \texttt{T} \\
		\hline
		\texttt{T} & \texttt{F} & \texttt{F} \\
		\hline
		\texttt{F} & \texttt{T} & \texttt{F} \\
		\hline
		\texttt{F} & \texttt{F} & \texttt{F} \\
	\end{tabular}
	&
	\begin{tabular}{c | c | c}
		$\phi$     & $\psi$     & $\phi\ \lor\ \psi$ \\
		\hline
		\texttt{T} & \texttt{T} & \texttt{T} \\
		\hline
		\texttt{T} & \texttt{F} & \texttt{T} \\
		\hline
		\texttt{F} & \texttt{T} & \texttt{T} \\
		\hline
		\texttt{F} & \texttt{F} & \texttt{F} \\
	\end{tabular}
	&
	\begin{tabular}{c | c | c}
		$\phi$     & $\psi$     & $\phi\ \Rightarrow\ \psi$ \\
		\hline
		\texttt{T} & \texttt{T} & \texttt{T} \\
		\hline
		\texttt{T} & \texttt{F} & \texttt{F} \\
		\hline
		\texttt{F} & \texttt{T} & \texttt{T} \\
		\hline
		\texttt{F} & \texttt{T} & \texttt{T} \\
	\end{tabular} \\
	
	\begin{tabular}{c | c}
		$\phi$     & \verb|~| $\phi$ \\
		\hline
		\texttt{T} & \texttt{F} \\
		\hline
		\texttt{F} & \texttt{T} \\
	\end{tabular}
	&
	\begin{tabular}{c}
	$\top$ \\
	\hline
	\texttt{T}
	\end{tabular}
	&
	\begin{tabular}{c}
	$\bot$ \\
	\hline
	\texttt{F}
	\end{tabular} \\
	
	\end{tabular}
\end{center}
\caption{Definitions of logical connectives}
\end{figure}

The meaning of a formula is determined by the propositional atoms it has and 
the logical connectives between propositional atoms. It is easy to assign boolean values to each propositional atom. We call this intention as \emph{valuation or model}:

\begin{definition}
    \textbf{(Valuation/Model)} A valuation or model of a formula $\phi$ is an assignment of each propositional atom in $\phi$ to a truth value.
\end{definition}

We defined what a \emph{tautology} is in the first chapter informally. By using the formal definitions of the syntax and semantics in our formal system, we give a more precise definition:

\begin{definition}
     \textbf{(Tautology)} A tautology is a proposition that all possible valuations of this formula make it evaluate to true.    
\end{definition}


The logical connectives specify how we should evaluate the value of a proposition after we have known all truth values assigned to its propositional atoms. We define them as functions. For example, we define conjunction ($\land$) as a function which takes in two boolean values and returns one boolean value. The specification of all these functions is given in Figure 2.3. To get the meaning of the whole formula, we can first draw the syntax tree of it, and then we apply these functions on the tree in a bottom-up direction. For instance, to calculate the meaning of the formula $p_0 \Rightarrow (p_1 \land p_2 )$, we first draw the syntax tree (Figure 2.4) of it. Then we start from the bottom. Supposed that the meaning of $p_0$, $p_1$ and $p_2$ are \emph{true, false, true}, then we first look at the right sub-tree $p_1 \land p_2$ and the meaning of it should be \emph{false} based on the definition of $\land$ in Figure 2.3. After that we move up to the root node. According to the definition of $\Rightarrow$, the meaning of $p_0 \Rightarrow (p_1 \land p_2)$ is \emph{false} given that $p_0$ is \emph{true} and the meaning of the right sub-tree is \emph{false}. Similarly, we can calculate the meaning of any well-formed propositional formula in a structurally recursive way by first calculating the meaning of its subtrees recursively and then finding the meaning of the whole tree by using the definition of the logical connective on the node.

\begin{figure}[h]
\Tree [.$\Rightarrow$ $p_0$ [.$\land$ $p_1$ $p_2$ ] ]
\caption{Syntax tree example}
\end{figure}


\section{Completeness theorem}

With appropriate definitions of syntax and semantics of our system, the 
soundness proof and completeness
proof can be implemented more easily (please recall that we call them soundness part and completeness part of the completeness theorem).
In this part we summarise what the completeness theorem states and
briefly introduce the ideas of their proofs. The informal definitions we saw in the first chapter only talk about tautologies. In this section, we extend the informal definition to contexts, which means that one proposition can be true with some premises even though it is not a tautology. For example, $\psi$ itself cannot be proved unless we have premises $\phi \Rightarrow \psi$ and $\phi$. Therefore we can say that under the contexts where $\phi \Rightarrow \psi$ and $\phi$ are true, $\psi$ is always true. 

Before we formally give
the completeness theorem, we need to specify how to interpret some
fundamental concepts of the completeness theorem.

\begin{definition}
  \textbf{(Sequent)} A sequent is a set of formulas $\phi_1, \phi_2,...,
  \phi_n$ called premises and another formula $\psi$ which is called
  conclusion.
\end{definition}

\begin{definition}
  \textbf{(Proof tree/Syntactical entailment) }A proof tree of a sequent $\phi_1, \phi_2,..., \phi_n
  \vdash \psi$ is a tree with root $\psi$ where the nodes are
  propositional logic deduction rules and $\phi_1, ..., \phi_n$ can be
  leaves.
\end{definition}

\begin{definition}
  \textbf{(Context)} A context is a set of all the premises in a sequent. The
  sequent $\phi_1, \phi_2,..., \phi_n \vdash \psi$ can be written as
  $\Gamma \vdash \psi$ where $\Gamma$ contains $\phi_i$ for all
  $1\ \leq\ i\ \leq\ n$. When the sequent only contains a theorem as
  its conclusion, the context is an empty collection $\varnothing$,
  e.g. $\varnothing \vdash \psi$.
\end{definition}

\begin{definition}
  \textbf{(Validity)} A syntactical entailment relation/proof tree is valid if a proof for it can be found. Usually we use ``$\Gamma \vdash \psi$ is valid'' to mean that we can absolutely find a way of constructing its proof tree by using deduction rules. Sometimes we also use ``$\Gamma \vdash \psi$ is provable'' or ``$\Gamma \vdash \psi$ has a proof'' as alternative expressions.
\end{definition}

\begin{definition}
  \textbf{(Semantic entailment)} If for all valuations (mappings from the
  propositional atoms to booleans) in which the context $\Gamma$
  evaluates to true, $\psi$ also evaluates to true, we
  say that $\Gamma$ semantically entails $\psi$. It can be written as
  $\Gamma \vDash \psi$.
\end{definition}

Informally speaking, the soundness and completeness with contexts can be stated as follow:

\begin{description}
\item[(Soundness)] If a propositional formula has a proof deduced from
  the given premises, then all assignments of the premises which make
  them evaluate to true also make the formula evaluate to true.
\item[(Completeness)] If for all assignments of the axioms and
  premises which make them evaluate to true also make a propositional
  formula true, then it is always possible to construct a proof of
  this formula by applying the deduction rules on the given premises.
\end{description}

With the interpretations above, we can rewrite them in formal mathematical language with logical symbols:

\begin{theorem}
	(Soundness) Let $\phi_1, \phi_2,..., \phi_n$ and $\psi$ be propositional logic formulas. If $\phi_1, \phi_2,..., \phi_n \vdash \psi$ is valid, then $\phi_1, \phi_2,..., \phi_n \vDash \psi$ holds.
\end{theorem}

\begin{theorem}
	(Completeness) Let $\phi_1, \phi_2,..., \phi_n$ and $\psi$ be propositional logic formulas. If $\phi_1, \phi_2,..., \phi_n \vDash \psi$ holds, then $\phi_1, \phi_2,..., \phi_n \vdash \psi$ is valid.
\end{theorem}

The completeness theorem consists of both. Therefore, the corollary
can be stated as follows:

\begin{theorem}
(Completeness Theorem) Let $\phi_1, \phi_2,..., \phi_n, \psi$ be formulas of 
propositional logic. $\phi_1, \phi_2,..., \phi_n \vDash \psi$ holds iff the 
sequent $\phi_1, \phi_2,..., \phi_n \vdash \psi$ is valid.
\end{theorem}

\subsection{Soundness proof}

This section briefly demonstrates how to prove the soundness of classical propositional logic. The detailed implementation will be presented in the next chapter along with the Agda code because it can be read as mathematical proofs.

The soundness part asserts $\Gamma \vdash \psi \rightarrow \Gamma
\vDash \psi$ where $\Gamma$ consists of $\phi_1,
\phi_2,...,\phi_n$: if we can find a proof tree $\Gamma \vdash \psi$, then if everything in $\Gamma$ is true, $\psi$ is also true. In order to prove it, we study the structure of $\Gamma \vdash \psi$ because the truth value of a proposition is determined by how its propositional atoms are connected by logical connectives and we can have a clear view of that once we obtain the structure of its proof tree. To illustrate this intention, we build a proof tree:

\begin{figure}[h]
\begin{prooftree}
    \AxiomC{$\phi_1$}
    \noLine
    \UnaryInfC{$\vdots$}
    \noLine
    \UnaryInfC{$\phi_n$}
    \noLine
    \UnaryInfC{$\vdots$}
    \UnaryInfC{$\psi$}    
\end{prooftree}
\caption{A proof tree to illustrate $\Gamma \vdash \psi$}
\end{figure}

At the beginning, one may believe that this proof tree contains so many uncertainties like what is happening in those dots and how we can know the inner structure of an arbitrary $\psi$. The truth is that we do not care about what is inside the proof tree. We use a proof method called \emph{structural induction} to solve the uncertainties. To understand it, considering how we do mathematical induction on a natural number, we only care about the base case which is about 0 or 1 and the previous case which talks about $n$ and we use them to prove the $n + 1$ case. The general idea is to use the ``smaller'' case $n$ to prove the ``larger'' case $n + 1$.

Likewise, in the structural induction here, we only care about how to use the previous smaller cases to prove the current larger case. To obtain these ``previous smaller cases'', we look \emph{one step} back at how $\Gamma \vdash \psi$ is constructed. For example, if the least significant connective in $\psi$ is $\land$, then that means we can expand $\psi$ as $\phi_1 \land \phi_2$. We stop at this point and do not expand $\phi_1$ or $\phi_2$ further. $\Gamma \vdash \phi_1$ and $\Gamma \vdash \phi_2$ are both smaller structures than $\Gamma \vdash \psi$. Therefore the ``smaller'' cases talk about them: $\Gamma \vdash \phi_1 \rightarrow \Gamma \vDash \phi_1$ and $\Gamma \vdash \phi_2 \rightarrow \Gamma \vDash \phi_2$. We can call them \emph{induction hypothesis} and we want to use them to prove the ``larger'' current case: $\Gamma \vdash \psi \rightarrow \Gamma \vDash \psi$. 

Now we notice that to use this structural induction, we need one systematical way to enumerate all possible results so that we will not miss any situation when we \emph{look one step back}. This can be achieved by checking the \emph{last rule} the proof tree $\Gamma \vdash \psi$ uses since those are the only ways to construct this proof tree. By following each rule in the Figure 2.1, we will not miss any situation in our logical system.

In our example, we actually checked the conjunction ($\land$) introduction rule which is the only way to construct propositions which have the form $\phi_1 \land \phi_2$. Therefore we know that inside those dots in the previous proof tree figure we can get two subtrees of the two components that $\psi$ has: $\Gamma \vdash \phi_1$ and $\Gamma \vdash \phi_2$. The expanded proof tree should look like:

\begin{figure}[h]
\begin{prooftree}
    \AxiomC{$\phi_3$}
    \noLine
    \UnaryInfC{$\vdots$}
    \noLine
    \UnaryInfC{$\phi_n$}
    \noLine
    \UnaryInfC{$\vdots$}
    \noLine
    \UnaryInfC{$\phi_1$}
    \AxiomC{$\phi_3$}
    \noLine
    \UnaryInfC{$\vdots$}
    \noLine
    \UnaryInfC{$\phi_n$}
    \noLine
    \UnaryInfC{$\vdots$}
    \noLine
    \UnaryInfC{$\phi_2$}
    \RightLabel{$\land$-i}
    \BinaryInfC{$\phi_1 \land \phi_2$ (= $\psi$)}    
\end{prooftree}
\caption{An expanded proof tree}    
\end{figure}

Then by the induction hypothesis we have shown above, we can prove that $\Gamma \vDash \phi_1$
and $\Gamma \vDash \phi_2$ both hold. We use them to prove $\Gamma
\vDash \phi_1 \land \phi_2$ (which is $\Gamma \vDash \psi$) by discussing how
the logical connective ($\land$) handles truth values: we know if everything in $\Gamma$ is true then $\phi_1$ and $\phi_2$ are both true. Then according to the definition of ($\land$) in the Figure 2.3, we know that $\phi_1 \land \phi_2$ is true. Therefore we can say that given $\Gamma \vdash \psi$, when everything in $\Gamma$ is true, $\psi$ is also true. In this way we have proved $\Gamma \vdash \psi \rightarrow \Gamma \vDash \psi$. The discussion on other deduction rules is similar to this example. The completed analysis of each case is in the Appendix. In Agda, this structural induction is implemented by pattern matching.

We give a general conclusion of the method used to prove soundness: first we have a proof tree $\Gamma \vdash \psi$ as our premise and we want to prove that $\Gamma \vDash \psi$ holds. We check the last deduction rule in the proof tree. We obtain the subformulas of $\psi$ ($\phi_1$ and $\phi_2$ in the example above) and their proof trees. By using the induction hypothesis, we simply prove the subformulas are \emph{true} in the context. Then we combine these \emph{true} subformulas together to form our original formula by using the logical connective we found before. Further, we use the semantical definition of the connective to prove that the original formula is also true in the given context. In the end, we can prove the soundness of classical propositional logic by exhausting all possible situations.


\subsection{Completeness proof}

This section presents the idea of how to prove the completeness part. The completeness part asserts: $\Gamma \vDash \psi \rightarrow \Gamma
\vdash \psi$ where $\Gamma$ consists of $\phi_1,
\phi_2,...,\phi_n$. As is mentioned before, we follow the general idea of Huth and Ryan's work \cite{CUP_book}, but the core part which involves the use of the law of the excluded middle is original because Huth and Ryan did not explain it in their book. Like the previous section, this section only presents the brief idea of how to prove it.

This part is much more difficult than the soundness part. I can attempt to give a brief explanation of why this is the case. When proving the soundness, even though we do not know what exactly the inner structure of the proposition is, we have a powerful premise that its proof tree ($\Gamma \vdash \psi$) exists for sure. Since there are only finite ways of constructing the proof tree, we just need to discuss them one by one to get substructures. Then the structural induction can handle the uncertainty for us. When proving the completeness, we do not have a powerful premise like $\Gamma \vdash \psi$ which has a well shaped mathematical structure. One may argue that we still can check the form of $\psi$ to try to find the proof tree $\Gamma \vdash \psi$. However, the loophole is that we can only know whether a specific $\Gamma \vdash \psi$ can be proved until we analyse the whole structure of it. For example, if we only have a $\Gamma \vdash p_0$ in which $p_0$ is just a propositional atom, then it can be deduced from either disjunction elimination rule or implication elimination rule or one of conjunction elimination rules. In soundness, when we look one step back we know which rule it uses since the proof tree structure is given as a premise so we can get any information we want. In completeness, we are trying to construct it so when we look one step back again we only see many possibilities and feel at a loss. We can only know how to prove it unless we look deeper. Therefore we cannot use induction to handle it since we cannot look only one step back. Moreover, simply using the proof construction method to prove completeness does not use the given premise $\Gamma \vDash \psi$ and cannot solve the problem elegantly. We cannot exhaust all  possible $\psi$s since there are infinitely many situations. Even if all proof trees with the form $\Gamma \vdash \psi$ we meet can be constructed when given $\Gamma \vDash \psi$, it is still ``possible'' that there might exist a very special $\psi$ such that $\Gamma \vDash \psi$ but $\Gamma \nvdash \psi$. Through this analysis, we realise that the idea of proving completeness by attempting to construct the proof tree for any $\Gamma \vdash \psi$ cannot solve the problem due to the infinity of propositions. Our goal is to find a uniform way of constructing proofs for any $\Gamma \vdash \psi$ without having to look at the inner structure and also we need to find a way to use induction idea to handle the infinity in this problem.

From the analysis above, we can gain an intuition about the difficulty of proving the completeness theorem: it has so many possibilities to construct the proof if we only see one step further, but it is impossible for us to exhaust all possible constructions for all arbitrary propositions since there are infinitely many of them. Thus, in order to come up with a universal method to construct a proof for any arbitrary proposition, we need to find their common characteristic and some ways of discussing it with only finite possible situations. Based on this idea, we realised that the break point is to use the fact that for any $\psi$ given $\Gamma \vDash \psi$, every line in its truth table evaluates to true. Even more, there are only at most $2^n$ lines in the truth table if $\psi$ contains $n$ propositional variables. Although this might be very large, it is finite after all. An expanded explanation will be given in the next chapter. Here we only give the overview of how to prove it:

\begin{description}
	\item [Lemma 1.] Our first step is to prove the theorem: $\phi_1, \phi_2,...,\phi_n \vDash \psi \rightarrow \varnothing \vDash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$. This states that if $\phi_1, \phi_2,...,\phi_n$ semantically entails $\psi$ then $\phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$ is a \emph{tautology}.  
	\item [Lemma 2.] In this step, the \emph{weak version} of completeness which is $\varnothing \vDash \eta \rightarrow \varnothing \vdash \eta$ is proved. The difference between it and the original one is that there are no premises in the context. This weaker one asserts that \emph{all tautologies have a proof}. By proving this, we can demonstrate that $\varnothing \vDash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi \rightarrow \varnothing \vdash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$ since we already proved that $\phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$ is a tautology in Lemma 1.
	\item [Lemma 3.] The last step is: $(\varnothing \vdash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi) \rightarrow (\phi_1, \phi_2,...,\phi_n \vdash \psi)$. Given $\varnothing \vdash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$, we add in $\phi_1, \phi_2,...,\phi_n$ as premises and we use implication ($\Rightarrow$) elimination rule to reduce the proposition from $\phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$ to $\psi$. In the end we can obtain $\phi_1, \phi_2,...,\phi_n \vdash \psi$.
\end{description}

As is mentioned in the beginning of section 2.3, the completeness of classical propositional logic here is more general since it contains contexts. The Lemma 1 and Lemma 3 above are just used to extend the weak completeness which is Lemma 2 to contexts. Lemma 2 is just the completeness theorem we introduced in chapter 1 since we did not consider contexts at that point. By combining these three proofs together we can prove the completeness theorem. If we take a closer look at them, the can be connected like pipes, i.e. the conclusion of the previous lemma is the premise of the current one and the conclusion of the current lemma is the premise of the next one. 

The most fantastic mathematical proof lies in Lemma 2. We will introduce it in every detail in the next chapter along with Agda code and not explain it here to avoid duplicate explanations. Readers should try to understand all the general ideas in this chapter to help with feeling how amazing the entire proof is in the next chapter.