\chapter{Introduction}

In this first chapter, the overall description of the project is presented. It attempts to use plain English to describe an interesting theoretical computer science and mathematical problem -- \emph{the completeness theorem of classical propositional logic}. It then explains the initial motivation of this project and the related work showing the background research. We will also present some naive definitions for some concepts to give readers a taste of what this dissertation is going to demonstrate and more rigorous definitions can be found in the next chapter -- background knowledge.

\section{Description of the Work}
This dissertation describes the implementation of the completeness theorem for classical propositional logic in a proof assistant -- Agda. In this section, the brief definitions of classical propositional logic and its soundness and completeness will be presented. 

\subsection{Classical Propositional logic}
There are many logical systems in mathematics and computer science. People use them to reason about everything in this world. In this dissertation, we focus on the most basic logical system -- \emph{propositional logic} or we can call it \emph{zeroth order logic and sentential logic}. More specifically, we use \emph{classical propositional logic} in this dissertation. It makes abstractions of real world statements and uses propositional variables like $P, Q$ or $p_0, p_1...$ to represent them.  It then studies how values of propositions depend on the truth values of their components. The word \emph{classical} means that we can assert the meaning of every proposition in our logical system is either true or false without any prior knowledge. The reason why people call it classical is that this conforms with the natural way that people think about objects in this world at the beginning: \emph{everything is either true or false, and there are no undecidable cases in the middle}. 

\subsection{Brief explanations of soundness and completeness}
To make sure that one system is rigorous and is safe to be used for reasoning, some standards have been set to evaluate it. Among all of these criteria, two basic things attract most researchers: \emph{soundness and completeness}. Together they are named as: completeness theorem. This is confusing since the second theorem itself can be called as the completeness theorem. In order to distinguish them, we call these two theorems the soundness part and the completeness part of the completeness theorem \cite{Van_Dalen} \cite{Johnstone} in the rest of this dissertation. The rigorous mathematical definitions will be in the next chapter. In this introductory section, we only have a look at an example to get the intuition of what soundness and completeness are. We first define a terminology: \emph{tautology}.

\begin{definition}
\textbf{(Tautology)} A tautology is a proposition which is always true no matter what truth values we assign to the propositional variables it contains. 
\end{definition}

In the later chapter the precise definition of a tautology will be given in the logical system we build. We now have a look at a \emph{tautology} in classical propositional logic: (P $\Rightarrow$ Q) $\land$ P $\Rightarrow$ Q. Please note that $\land$ has higher priority than $\Rightarrow$. One brute force way of checking whether it is a tautology would be to replace the propositional variables P and Q with boolean values ``true'' and ``false''. If readers do not know how to do propositional calculus, it does not matter. It is enough to understand the idea here: we enumerate every combination of the truth values assigned to P and Q, then we see if this proposition always evaluates to true to judge whether it is a tautology. We use truth table to fulfil this task:

\begin{figure}[h]
\begin{center}
\begin{tabular}{| l | l | l | l | l |}
\hline
P & Q & (P $\Rightarrow$ Q) & (P $\Rightarrow$ Q) $\land$ P & (P $\Rightarrow$ Q) $\land$ P $\Rightarrow$ Q \\
\hline
T & T & T & T & T \\
T & F & F & F & T \\
F & T & T & F & T \\
F & F & T & F & T \\
\hline
\end{tabular}
\end{center}
\caption{Truth table example}
\end{figure}

By definition, it is a tautology. However, some readers may not like this brute force way since if there are 100 components in the proposition the truth table will become extremely large ($2^{100}$). Indeed, there is an easier way to justify that this proposition is a tautology. We assume P and Q are some real life propositions and we study the \emph{modus ponens} rule: 

\begin{figure}[h]
\begin{center}
\begin{tabular}{l l l}
Premise 1 & (P $\Rightarrow$ Q) & : If it's raining, then it's cloudy. \\
Premise 2 & (P) & : It's raining. \\
Conclusion & (Q) & : It's cloudy.
\end{tabular}
\end{center}
\caption{Modus ponens}
\end{figure}

From the Figure 1.2 above, we notice that no matter what real life propositions we give to P and Q, as long as they have forms ``If P, then Q'' for the premise 1 and ``P'' for the premise 2 we will always get ``Q'' as our conclusion. The proposition (P $\Rightarrow$ Q) $\land$ P $\Rightarrow$ Q is exactly what the \emph{modus ponens} says: we have premise 1 and premise 2 which have the required forms above, then we can get the corresponding conclusion. Since we admit that the modus ponens rule is always right, then this proposition always states a fact and thus is a tautology.

Some clever readers may notice that the trick we use in the second method: we admit that the modus ponens rule is always right and prove the equivalence between this fact and the proposition we want to prove. Then we claim that the proposition is also a fact, in other words, a tautology. During the whole process, we never discuss the truth values of P and Q. We only discuss that the proposition is constructed based on the same idea which the modus ponens rule uses.

Likewise, in propositional logic, there are also some other deduction rules that we presume as right. Then we try to use them to construct the proposition we want to prove. If we succeed, then the proposition is a tautology. Otherwise it is not. 

\begin{remark}
If readers know about the natural deduction rules in the propositional logic. The construction of the proposition (P $\Rightarrow$ Q) $\land$ P $\Rightarrow$ Q in this example uses not only modus ponens (which we may call as implication elimination rule in propositional logic), but also uses conjunction elimination rule and implication introduction rule. The full proof construction of this example will be shown in the next chapter after we introduce the deduction rules in our logical system. The most important thing is to give readers a feeling that we can prove tautologies by using some rules to help with constructing it instead of using truth tables to enumerate all truth value combinations.
\end{remark}

We now have seen two ways of proving a propositional formula is true. One is to use the truth table and check the meaning of it, and the other one is to inspect whether we can construct it by using deduction rules. The key question is: \emph{are these two things equivalent?} The answer is yes, and this is exactly what soundness and completeness say. We present very informal definitions of them in this section. More rigorous definitions will be introduced in the next chapter. 

\begin{description}
    \item [soundness:] If we can construct a proposition by using deduction rules, then the proposition always evaluates to true in its truth table for all the combinations of truth values of its components.
    \item [completeness:]  If the truth table shows that a proposition is always true no matter what truth values it uses for its propositional components, then we must be able to find a way to construct it by using deduction rules.
\end{description}

We say that a proposition can be ``deduced'' if we can construct it by applying deduction rules finite times. If a logical system satisfy the two criteria above, then we know that \emph{the system will never deduce false statements (due to soundness) since everything it deduces must evaluate to true} and \emph{all true propositions can be deduced from it (due to completeness) because they must have a way to be constructed/deduced by applying deduction rules in the logical system}. Such a logical system would be very safe to use since it eliminates all absurd things and includes all reasonable things.

\subsection{The main objective of this project}

In general, the main topic in this dissertation is to prove that classical propositional logical system satisfies soundness and completeness. This goal is achieved by using a computer proof assistant -- Agda. The mathematical proof itself has been done by antecedent mathematicians and logicians in many ways. Among all these ideas, this dissertation chooses Huth and Ryan's work \cite{CUP_book} as a guide.

The most interesting but difficult part is that when proving the completeness part, we are actually building an algorithm which can always return the construction method (later in this dissertation we also call it the proof tree) for any arbitrarily given proposition. The fact that we can definitely find this algorithm is relevant to what is mentioned in the last sentence in the section 1.1.1: \emph{whether a proposition is true in classical propositional logic is decidable}. 

By interactively implementing the proof in Agda, we can attain deeper understanding of how to use a universal method to construct formal proofs for arbitrary propositions from their propositional atoms. Even more, we can understand the computational content beneath the mathematical logic calculus.

\newpage
In sum, the objectives are as follow:

\begin{enumerate}
    \item Formalising classical propositional logical system in which we can prove theorems in Agda.
    \item Constructing mathematical proofs for the soundness and completeness of classical propositional logic.
    \item Explaining the background theory while constructing proofs.
\end{enumerate}

It seems that there are not many things to do. However, proving a theorem in a functional programming language requires programmers to fully understand the mathematical proof and be aware of all the computational content especially how the elements in a set are operated. Even the logical system should be implemented from the scratch. Therefore none of the points above is easy to achieve.

\section{Motivation}

\subsection{Motivation of the project}
The initial motivation of this work comes from the lectures in my undergraduate course. The conclusion that classical propositional logic is sound and complete was directly introduced to us without detailed explanations. Therefore I personally want to find out how to prove it. Later I found Huth and Ryan's work 
\cite{CUP_book}. They provide the framework of how to prove the soundness and completeness
of classical propositional logic. However, their explanation does not 
include every detail and one may still get stuck when trying to understand 
the philosophy underneath. For example, the core of the whole proof is to use
\emph{the law of the excluded middle} to construct
sufficient premises to prove a given \emph{true} proposition. In their book, 
they only present a simple case which gives readers a taste of how to use the
law of the excluded middle without giving comprehensive proof. Therefore,
one aim of this dissertation is to fill in the blanks that Huth and Ryan did 
not explain so as to finish the work. It not only demonstrates a machine checked proof of the
completeness theorem of classical propositional logic, but also presents an 
accessible way of writing this proof which can serve for educational purposes.

Another source of the motivation of this work is that some other completeness
theorems of formal logical systems in Agda have been formalised by other 
researchers. The proof in this dissertation is not as difficult as what other
people have done, but the fact is no one has done this before. More detailed
related work will be introduced in the section 1.3.

\subsection{Motivation of choosing Agda}

Indeed there are many proof assistants which can be used. For example, Coq, Isabelle, and Epigram. They all work perfectly for this project. However, Agda has some advantages which none of the others have. Fisrt, it supports Unicode. Briefly speaking, programmers can use Greek letters like $\sigma$ and $\delta$ as variables in Agda code, which makes programs look like real mathematical proofs. Secondly, it has implicit arguments mechanism. A programming language like Java requires programmers to feed in all parameters when doing function calls. In contrast, Agda provides a convenient way of it -- it can set implicit arguments in its function declarations. With this feature, programmers do not have to write all arguments for a function if some of them are set to be implicit and Agda can automatically ``guess'' what they are. This can provide clean and more readable code. The third feature of Agda is that it does not have built-in tactics for logicians to use like what Coq and Isabelle do. It is only a huge framework with no content inside, which provides programmers with significant freedom.

\section{Related Work}
The context of this work is formalising completeness theorems of
formal logical systems in Agda. Formalising the completeness of
intuitionistic logic with Kripke model in a proof assistant has been
finished by a number of researchers. Herbelin and Lee presented an
implementation of the completeness of Kripke model for intuitionistic
logic with implication and universal quantification in Coq
\cite{Kripke_Coq}. Blanchette et al. construct the completeness
theorem of first-order logic by using Gentzen's system and codatatypes
\cite{blanchette}. Coquand presents a fully formalised proof of the
soundness and completeness of simply-typed $\lambda$-calculus with
respect to Kripke models \cite{catarinacoquand}.

The formalisation in this dissertation focuses on classical propositional logic 
in \emph{natural deduction style} and prove its soundness and completeness with 
regards the \emph{boolean model}. In the future, I may attempt to formalise other 
completeness theorems for nonstandard Kripke model and Beth model of the
intuitionistic full first-order predicate logic (\cite{Veldman},
\cite{Friedman}). The motivation will be discussed along with the discussion of the current formalisation in the final chapter of this dissertation.

The formalisation takes place in a constructive metalanguage of Agda,
which means that the proof has computational content. The
corresondance between classical logic and computation has been first
noticed by Griffin \cite{Griffin90aformulae-as-types}. A good summary
of the developments in this area is Urban's PhD thesis
\cite{urban2000classical}.
