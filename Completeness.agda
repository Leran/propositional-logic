open import Data.Nat

module Completeness (n : ℕ) where

open import Relation.Nullary
open import Relation.Nullary.Decidable -- as Dec
open import Data.Bool renaming (_≟_ to _B≟_; _∧_ to _`and`_ ; _∨_ to _`or`_)
open import Data.Empty renaming (⊥-elim to exfalso ; ⊥ to E⊥)
open import Data.Fin renaming (_+_ to _F+_; _<_ to _F<_; _≤_ to _F≤_; pred to fpred)
open import Data.Fin.Properties renaming (_≟_ to _F≟_)
open import Data.Nat.Properties
open import Data.Product
open import Relation.Binary renaming (_⇒_ to _Bi⇒_)
open import Relation.Binary.PropositionalEquality renaming (sym to _⁻¹; trans to _◾_)
open import Function

open import Library

open import Syntax n
open import Semantics n


-----------------------------------------------------------------------------------------------------
-- Main structure
-----------------------------------------------------------------------------------------------------

completeness : ∀{l}{Γ : Cxt l}{φ : Props} → Γ ⊨ φ → Γ ⊢ φ

_⇛_          : ∀{l}(Γ : Cxt l)(ψ : Props) → Props

lemma1       : ∀{l}{Γ : Cxt l}{ψ : Props} → Γ ⊨ ψ → ∅ ⊨ Γ ⇛ ψ

lemma2       : ∀{η : Props} → ∅ ⊨ η → ∅ ⊢ η

lemma3       : ∀{l}{Γ : Cxt l}{ψ : Props} → ∅ ⊢ (Γ ⇛ ψ) → Γ ⊢ ψ

completeness {Γ = Γ} = lemma3 ∘ lemma2 ∘ lemma1 {Γ = Γ}

-----------------------------------------------------------------------------------------------------
-- lemma1 : Γ ⊨ ψ → ∅ ⊨ Γ ⇛ ψ
-----------------------------------------------------------------------------------------------------

∅       ⇛ ψ = ψ
(Γ ∙ φ) ⇛ ψ = Γ ⇛ φ ⇒ ψ

infix 6 _⇛_

lemma1 {Γ = ∅}     {ψ} ∅⊨ψ ρ ⟦∅⟧≡true = ∅⊨ψ ρ ⟦∅⟧≡true
lemma1 {Γ = Γ ∙ φ} {ψ} ∅⊨ψ ρ ⟦∅⟧≡true = lemma1 {Γ = Γ} (λ ρ' → oneStep ρ' (∅⊨ψ ρ')) ρ refl

  where
    oneStep : ∀ ρ → (⟦ Γ ∙ φ ⟧ᶜ ρ ≡ true → ⟦ ψ ⟧ ρ ≡ true) → (⟦ Γ ⟧ᶜ ρ ≡ true → ⟦ φ ⇒ ψ ⟧ ρ ≡ true)

    oneStep ρ σ γt with ⟦ φ ⟧ ρ
    oneStep ρ σ γt | true with ⟦ ψ ⟧ ρ
    oneStep ρ σ γt | true | true  = refl
    oneStep ρ σ γt | true | false = σ (subst (λ x → x `and` true ≡ true) (γt ⁻¹) refl)
    oneStep ρ σ γt | false = refl

-----------------------------------------------------------------------------------------------------
-- lemma2 : ∅ ⊨ η → ∅ ⊢ η
-----------------------------------------------------------------------------------------------------


lemma2 = lemma22 ∘ lemma21

  where

    Cxt[_] : ∀ ρ {a} → a ≤ n → Cxt a

    lemma21 : ∀{η : Props} → ∅ ⊨ η → (∀ ρ → Cxt[ ρ ] ≤-refl ⊢ η)

    lemma22 : ∀{η : Props} → (∀ ρ → Cxt[ ρ ] ≤-refl ⊢ η) → ∅ ⊢ η

    -------------------------------------------------------------------------------------------------
    -- lemma21 : ∅ ⊨ ψ → (∀ ρ → Cxt[ ρ ] ≤-refl ⊢ ψ)
    -------------------------------------------------------------------------------------------------

    p̂ᵢ[_] : ∀ ρ → Fin n → Props
    p̂ᵢ[ ρ ] x with ρ x
    p̂ᵢ[ ρ ] x | true  =   patom x
    p̂ᵢ[ ρ ] x | false = ~ patom x

    Cxt[_] _ {zero} z≤n = ∅
    Cxt[_] ρ {suc a} α = Cxt[ ρ ] {a} (≤⇒pred≤ _ _ α) ∙ p̂ᵢ[ ρ ] (fromℕ≤ α)

    lemma21 σ ρ = lemma211 ρ (σ ρ refl)

      where

        lemma211 : ∀{ψ} ρ → (⟦ ψ ⟧ ρ ≡ true)  → (Cxt[ ρ ] ≤-refl) ⊢ ψ
        lemma212 : ∀{ψ} ρ → (⟦ ψ ⟧ ρ ≡ false) → (Cxt[ ρ ] ≤-refl) ⊢ ~ ψ

        ---------------------------------------------------------------------------------------------
        -- lemma211 : (⟦ ψ ⟧ ρ ≡ true) → (Cxt[ ρ ] ≤-refl) ⊢ ψ
        ---------------------------------------------------------------------------------------------
        
        lemma211 {ψ = ⊥}       ρ ()
        lemma211 {ψ = ⊤}       ρ _ = ⊤-i {Γ = Cxt[ ρ ] ≤-refl}
                             
        lemma211 {ψ = patom x} ρ ⟦ψ⟧≡true = Cxt[ρ]⊢patom ρ ⟦ψ⟧≡true n (bounded _) ≤-refl

          where

            Cxt[ρ]⊢patom : ∀ ρ {x : Fin n} → ρ x ≡ true → (y : ℕ) → toℕ x < y
                         → (y≤n : y ≤ n) → (Cxt[ ρ ] y≤n) ⊢ patom x
            Cxt[ρ]⊢patom ρ     e zero   () β
            Cxt[ρ]⊢patom ρ {x} e (suc y) α β with x F≟ fromℕ≤ β
            ... | yes p rewrite subst (λ z → ρ z ≡ true) p e = subst (λ z → _ ⊢ patom z) (p ⁻¹) var
            ... | no ¬p = weaken (Cxt[ρ]⊢patom ρ e y (lemma-≤1 α (¬p ∘ toℕ→fromℕ)) (≤⇒pred≤ _ _ β))

        lemma211 {ψ = ~ φ}     ρ pt with ⟦ φ ⟧ ρ | inspect ⟦ φ ⟧ ρ
        lemma211 {ψ = ~ φ}     ρ () | true  | [ eq ]
        lemma211 {ψ = ~ φ}     ρ pt | false | [ eq ] = lemma212 ρ eq 
                             
        lemma211 {ψ = φ₁ ∨ φ₂} ρ pt with ⟦ φ₁ ⟧ ρ | inspect ⟦ φ₁ ⟧ ρ
        lemma211 {ψ = φ₁ ∨ φ₂} ρ pt | true  | [ eq ] = ∨-i₁ (lemma211 ρ eq)
        lemma211 {ψ = φ₁ ∨ φ₂} ρ pt | false | [ eq ] = ∨-i₂ (lemma211 ρ pt)
                             
        lemma211 {ψ = φ₁ ∧ φ₂} ρ _  with ⟦ φ₁ ⟧ ρ | inspect ⟦ φ₁ ⟧ ρ | ⟦ φ₂ ⟧ ρ | inspect ⟦ φ₂ ⟧ ρ
        lemma211 {ψ = φ₁ ∧ φ₂} ρ _  | true  | [ φ₁≡true ] | true | [ φ₂≡true ]
                             
          = ∧-i (lemma211 ρ φ₁≡true) (lemma211 ρ φ₂≡true)
                             
        lemma211 {ψ = φ₁ ∧ φ₂} ρ () | true  | [ _ ] | false | [ _ ]
        lemma211 {ψ = φ₁ ∧ φ₂} ρ () | false | [ _ ] | _ | [ _ ]

        lemma211 {ψ = φ₁ ⇒ φ₂} ρ pt with ⟦ φ₂ ⟧ ρ | inspect ⟦ φ₂ ⟧ ρ
        lemma211 {ψ = φ₁ ⇒ φ₂} ρ pt | true | [ eq ] = ⇒-i (weaken (lemma211 ρ eq))
        lemma211 {ψ = φ₁ ⇒ φ₂} ρ pt | false | [ eq ] with ⟦ φ₁ ⟧ ρ | inspect ⟦ φ₁ ⟧ ρ
        lemma211 {ψ = φ₁ ⇒ φ₂} ρ () | false | [ eq ] | true | [ eq₁ ]
        lemma211 {ψ = φ₁ ⇒ φ₂} ρ pt | false | [ eq ] | false | [ eq₁ ]

          = ⇒-i
              (⊥-e
                (~-e
                  var
                  (weaken (lemma212 ρ eq₁))))

        ---------------------------------------------------------------------------------------------
        -- lemma212 : (ρ : Val) → (⟦ ψ ⟧ ρ ≡ false) → (Cxt[ ρ ] ρ n ≤-refl) ⊢ ~ ψ
        ---------------------------------------------------------------------------------------------
                             
        lemma212 {ψ = ⊥}       ρ b = ~-i var
        lemma212 {ψ = ⊤}       ρ ()
        lemma212 {ψ = patom x} ρ xt = Cxt[ρ]⊢~patom ρ x xt _ (bounded _) ≤-refl

          where
            
            Cxt[ρ]⊢~patom : ∀ ρ (x : Fin n) → ρ x ≡ false → (y : ℕ) → toℕ x < y
                          → (y≤n : y ≤ n) → (Cxt[ ρ ] y≤n) ⊢ ~ patom x
            Cxt[ρ]⊢~patom ρ x e zero   () β
            Cxt[ρ]⊢~patom ρ x e (suc y) α β with x F≟ fromℕ≤ β
            ... | yes p rewrite subst (λ z → ρ z ≡ false) p e = subst (λ z → _ ⊢ ~ patom z) (p ⁻¹) var
            ... | no ¬p = weaken (Cxt[ρ]⊢~patom ρ x e y (lemma-≤1 α (¬p ∘ toℕ→fromℕ)) (≤⇒pred≤ _ _ β))

        lemma212 {ψ = ~ φ}     ρ b  with ⟦ φ ⟧ ρ | inspect ⟦ φ ⟧ ρ
        lemma212 {ψ = ~ φ}     ρ b  | true  | [ eq ] = ~-i (~-e (weaken (lemma211 ρ eq)) var)

        lemma212 {ψ = ~ φ}     ρ () | false | [ eq ]
                             
        lemma212 {ψ = φ₁ ∨ φ₂} ρ b  with ⟦ φ₁ ⟧ ρ | inspect ⟦ φ₁ ⟧ ρ
        lemma212 {ψ = φ₁ ∨ φ₂} ρ () | true  | [ eq ]
        lemma212 {ψ = φ₁ ∨ φ₂} ρ b  | false | [ eq ]

          = ~-i
              (∨-e
                var
                (~-e
                  var
                  (weaken (weaken (lemma212 ρ eq))))
                (~-e
                  var
                  (weaken
                    (weaken (lemma212 ρ b)))))
                             
        lemma212 {ψ = φ₁ ∧ φ₂} ρ b  with ⟦ φ₁ ⟧ ρ | inspect ⟦ φ₁ ⟧ ρ | ⟦ φ₂ ⟧ ρ | inspect ⟦ φ₂ ⟧ ρ
        lemma212 {ψ = φ₁ ∧ φ₂} ρ () | true | [ eq ] | true | [ eq₁ ]
        lemma212 {ψ = φ₁ ∧ φ₂} ρ b  | true | [ eq ] | false | [ eq₁ ]

          = ~-i
              (~-e
                (∧-e₂
                  var)
                (weaken (lemma212 ρ eq₁)))
                
        lemma212 {ψ = φ₁ ∧ φ₂} ρ b  | false | [ eq ] | _ | [ _ ]

          = ~-i
              (~-e
                (∧-e₁
                  var)
                (weaken (lemma212 ρ eq)))
                             
        lemma212 {ψ = φ₁ ⇒ φ₂} ρ b  with ⟦ φ₁ ⟧ ρ | inspect ⟦ φ₁ ⟧ ρ
        lemma212 {ψ = φ₁ ⇒ φ₂} ρ b  | true | [ eq ]

          = ~-i
              (~-e
                (⇒-e
                  var
                  (weaken (lemma211 ρ eq)))
                (weaken (lemma212 ρ b)))
                
        lemma212 {ψ = φ₁ ⇒ φ₂} ρ () | false | [ eq ]

    -------------------------------------------------------------------------------------------------
    -- lemma22 : ∀{η : Props} → ((ρ : Val) → Cxt[ ρ ] ≤-refl ⊢ η) → ∅ ⊢ η
    -------------------------------------------------------------------------------------------------

    lemma22 {η} σ = lemma221 {β = z≤n} z≤n σ (λ _ → true)

      where
      
        lemma221 : ∀{η}{a b}{α : a ≤ n}{β : b ≤ n}(β' : b ≤ a)
                 → (∀ ρ → Cxt[ ρ ] α ⊢ η)
                 → (∀ ρ → Cxt[ ρ ] β ⊢ η)

        lemma222 : ∀{a}{α : a < n}{η : Props}
                 → (∀ ρ → Cxt[ ρ ] {suc a} α ⊢ η) → (∀ ρ → Cxt[ ρ ] {a} (≤⇒pred≤ _ _ α) ⊢ η)

        ---------------------------------------------------------------------------------------------
        -- lemma221 : (∀ ρ → Cxt[ ρ ] an ⊢ η) → (∀ ρ → Cxt[ ρ ] bn ⊢ η)
        ---------------------------------------------------------------------------------------------

        lemma221 {a = a}{b} β' σ ρ with a ≟ b
        lemma221 β' σ ρ | yes refl = subst (λ z → Cxt[ ρ ] z ⊢ _) (≤-unique _ _) (σ ρ)
        lemma221 {a = zero} z≤n  σ ρ | no ¬p = subst (λ x → Cxt[ ρ ] x ⊢ _) (≤-unique _ _) (σ ρ)
        lemma221 {a = suc a} β'  σ ρ | no ¬p = lemma221 (lemma-≤2 β' (¬p ∘ _⁻¹)) (lemma222 σ) ρ

        ---------------------------------------------------------------------------------------------
        -- lemma222 : (∀ ρ → Cxt[ ρ ] an ⊢ η) → (∀ ρ → Cxt[ ρ ] {a} (≤⇒pred≤ _ _ an) ⊢ η)
        ---------------------------------------------------------------------------------------------

        lemma222 {a = a} {α} {η} σ ρ
        
          = ∨-e (lem {ψ = patom (fromℕ≤ α)})
                (subst (λ z → z ⊢ η) (lemma-Cxt ρ α) (σ (ρ [ α ↦ true ])))
                (subst (λ z → z ⊢ η) (lemma-Cxt ρ α) (σ (ρ [ α ↦ false ])))

          where

            _[_↦_] : Val → {a : ℕ} → a < n → Bool → Val
            (ρ [ α ↦ b ]) x with fromℕ≤ α F≟ x
            ... | yes p = b
            ... | no ¬p = ρ x

            ~? : Bool → Props → Props
            ~? true φ = φ
            ~? false φ = ~ φ

            head-_[_↦_] : ∀ ρ {a}(α : a < n)(c : Bool) → c ≡ (ρ [ α ↦ c ]) (fromℕ≤ α)
            head- ρ [ α ↦ c ] with fromℕ≤ α F≟ fromℕ≤ α
            ... | yes p = refl
            ... | no ¬p = exfalso (¬p refl)

            head-p̂ : ∀ ρ {a}(α : a < n){c : Bool}
                   → p̂ᵢ[ ρ [ α ↦ c ] ] (fromℕ≤ α) ≡ ~? c (patom (fromℕ≤ α))
            head-p̂ ρ {a} α {c} with (ρ [ α ↦ c ]) (fromℕ≤ α) | inspect (ρ [ α ↦ c ]) (fromℕ≤ α)
            head-p̂ ρ α {true}  | true  | _     = refl
            head-p̂ ρ α {false} | true  | [ e ] with head- ρ [ α ↦ false ] ◾ e
            ...                                   | ()
            head-p̂ ρ α {true}  | false | [ e ] with head- ρ [ α ↦ true  ] ◾ e
            ...                                   | ()
            head-p̂ ρ α {false} | false | [ e ] = refl

            tail-p̂ : ∀ ρ {a b}(α : a < n)(β : b < a)(β' : suc b ≤ n){c : Bool}
               → p̂ᵢ[ ρ [ α ↦ c ] ] (fromℕ≤ β') ≡ p̂ᵢ[ ρ ] (fromℕ≤ β')
            tail-p̂ ρ α β β' with fromℕ≤ α F≟ fromℕ≤ β'
            tail-p̂ ρ α β β' | yes p = exfalso (lemma-fromℕ α β' p β)
            tail-p̂ ρ α β β' | no ¬p with ρ (fromℕ≤ β')
            tail-p̂ ρ α β β' | no ¬p | true  = refl
            tail-p̂ ρ α β β' | no ¬p | false = refl

            tails-Cxt : ∀ ρ {a b}(α : a < n)(β : b ≤ a)(β' : b ≤ n){c : Bool}
              → Cxt[ ρ [ α ↦ c ] ] β' ≡ Cxt[ ρ ] β'
            tails-Cxt ρ {b = zero} α β z≤n = refl
            tails-Cxt ρ {b = suc b} α β β'
              = Cxt= (tails-Cxt ρ {b = b} α (≤⇒pred≤ _ _ β) (≤⇒pred≤ _ _ β'))
                     (tail-p̂ ρ α β β')

            tail-Cxt : ∀ ρ {a}(α : a < n){c : Bool}
              → tail (Cxt[ ρ [ α ↦ c ] ] α) ≡ Cxt[ ρ ] (≤⇒pred≤ _ _ α)
            tail-Cxt ρ {zero} α = refl
            tail-Cxt ρ {suc a} α = tails-Cxt ρ α ≤-refl (≤⇒pred≤ _ _ α)

            lemma-Cxt : ∀ ρ {a}(α : a < n){c : Bool}
              → Cxt[ ρ [ α ↦ c ] ] α ≡ Cxt[ ρ ] (≤⇒pred≤ _ _ α) ∙ ~? c (patom (fromℕ≤ α))
            lemma-Cxt ρ {zero} α {true}  rewrite head-p̂ ρ α {true}  = refl
            lemma-Cxt ρ {zero} α {false} rewrite head-p̂ ρ α {false} = refl
            lemma-Cxt ρ {suc a} α = Cxt= (tail-Cxt ρ α) (head-p̂ ρ α)

-----------------------------------------------------------------------------------------------------
-- lemma3 : ∅ ⊢ (Γ ⇛ ψ) → Γ ⊢ ψ
-----------------------------------------------------------------------------------------------------

lemma3 {Γ = ∅}     {ψ} σ = σ
lemma3 {Γ = Γ ∙ φ} {ψ} σ = ⇒-e (weaken (lemma3 {Γ = Γ} σ)) var

p[_] : ∀ (ρ : Fin n → Bool) → Fin n → Props
p[ ρ ] x with ρ x
p[ ρ ] x | true  =   patom x
p[ ρ ] x | false = ~ patom x

