open import Data.Nat

module NBC where

open import Data.Fin
open import Function hiding (_$_)
open import Relation.Binary.PropositionalEquality

n : ℕ
n = 0

open import Syntax n
open import Soundness n
open import Completeness n

-----------------------------------------
-- abbreviations
-----------------------------------------

nbc : ∀{l}{Γ : Cxt l}{φ : Props} → Γ ⊢ φ → Γ ⊢ φ
nbc = completeness ∘ soundness

Λ : ∀{l}{Γ : Cxt l}{φ ψ} → Γ ∙ φ ⊢ ψ →  Γ ⊢ φ ⇒ ψ
Λ = ⇒-i

_$_ : ∀{l}{Γ : Cxt l}{φ ψ} → Γ ⊢ φ ⇒ ψ → Γ ⊢ φ → Γ ⊢ ψ
_$_ = ⇒-e

Tm : ∀{l}(Γ : Cxt l)(ψ : Props) → Set
Tm = _⊢_

* : ∀{l}{Γ : Cxt l} → Tm Γ ⊤
* = ⊤-i

q : ∀{l}{Γ : Cxt l}{ψ} → Γ ∙ ψ ⊢ ψ
q = var

_[p] : ∀{l}{Γ : Cxt l}{φ ψ} → Γ ⊢ ψ → Γ ∙ φ ⊢ ψ
_[p] = weaken

-----------------------------------------
-- examples
-----------------------------------------

t0 : Tm ∅ ⊤
t0 = Λ q $ *

t0' : nbc t0 ≡ *
t0' = refl

t1 : Tm ∅ ⊤
t1 = (Λ q $ Λ q) $ *

t1' : nbc t1 ≡ *
t1' = refl

t2 : Tm ∅ (⊤ ⇒ ⊤)
t2 = (Λ q $ Λ q) $ (Λ q $ Λ q)

t2' : nbc t2 ≡ Λ (* [p])
t2' = refl

t3 : Tm {0} ∅ (⊤ ⇒ ⊤)
t3 = Λ q

t3' : nbc t3 ≡ Λ (* [p])
t3' = refl
