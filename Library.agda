module Library where

open import Data.Empty renaming (⊥-elim to exfalso)
open import Data.Fin hiding (_≤_ ; _<_) renaming (pred to fpred)
open import Data.Nat
open import Data.Nat.Properties
open import Relation.Binary
open import Relation.Binary.PropositionalEquality

private module TO = DecTotalOrder decTotalOrder

≤-refl : {x : ℕ} → x ≤ x
≤-refl = TO.reflexive refl

lemma-≤1 : ∀ {l n} → suc l ≤ suc n → l ≢ n → suc l ≤ n
lemma-≤1 {zero} {zero} le σ = exfalso (σ refl)
lemma-≤1 {zero} {suc n} le σ = s≤s z≤n
lemma-≤1 {suc l} {zero} (s≤s ()) σ
lemma-≤1 {suc l} {suc n} (s≤s le) σ = s≤s (lemma-≤1 le (λ l≡n → σ (cong suc l≡n)))

lemma-≤2 : ∀ {l n} → l ≤ suc n → l ≢ suc n → l ≤ n
lemma-≤2 {zero} le nln = z≤n
lemma-≤2 {suc .0} {zero} (s≤s z≤n) nln = exfalso (nln refl)
lemma-≤2 {suc l} {suc n} (s≤s le) nln = s≤s (lemma-≤2 le (λ x → nln (cong suc x)))

≤-unique : ∀ {m n} → (p q : m ≤ n) → p ≡ q
≤-unique z≤n z≤n = refl
≤-unique (s≤s m≤n) (s≤s m≤n') = cong s≤s (≤-unique m≤n m≤n')

fsuc-inv : {n : ℕ}{p q : Fin n} → Data.Fin.suc p ≡ Data.Fin.suc q → p ≡ q
fsuc-inv refl = refl

toℕ→fromℕ : {n : ℕ}{x : Fin n}{y : ℕ}{β : suc y ≤ n} → toℕ x ≡ y → x ≡ fromℕ≤ β
toℕ→fromℕ {x = zero} {zero} {s≤s z≤n} p = refl
toℕ→fromℕ {x = zero} {suc y}          ()
toℕ→fromℕ {x = suc x}{zero}           ()
toℕ→fromℕ {x = suc x}{suc y}{s≤s β}   p
  = subst (_≡_ (suc x))
          (move-suc-out {β = β})
          (cong suc (toℕ→fromℕ (cong pred p)))
  where
    move-suc-out : {y n : ℕ}{β : suc y ≤ n} → suc (fromℕ≤ β) ≡ fromℕ≤ (s≤s β)
    move-suc-out {n = zero} {()}
    move-suc-out {n = suc n} {s≤s β} = refl

lemma-fromℕ : ∀{a b n}(α : a < n)(β : b < n) → fromℕ≤ α ≡ fromℕ≤ β → b < a → Data.Empty.⊥
lemma-fromℕ {suc a} {.0} {suc ._} (s≤s (s≤s α)) (s≤s z≤n) () (s≤s ba)
lemma-fromℕ {suc a} {._} {suc ._} (s≤s (s≤s α)) (s≤s (s≤s β)) e (s≤s ba)
  = lemma-fromℕ (s≤s α) (s≤s β) (fsuc-inv e) ba
