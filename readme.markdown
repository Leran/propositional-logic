The source code of the formalisation accompanying the paper

    Formalising the Completeness Theorem of Classical Propositional Logic in Agda (Proof Pearl)
    
    Leran Cai, Ambrus Kaposi, and Thorsten Altenkirch

can be found here: https://bitbucket.org/Leran/propositional-logic/src.

TODO:

 * beautify the Agda code:

    * Fin

    * rename ⟦cxt⟧≡true to something else in the soundness proof and
      similar changes

 * fix font of urls

 * change font size for Agda code

 * add note about _

 * NBC:

   * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.120.7944&rep=rep1&type=pdf
   
   * http://www.lix.polytechnique.fr/~lengrand/Work/Teaching/MPRI/lecture1.pdf
   
   * http://www.infres.enst.fr/~bellot/INF340/ARTICLES/Michel_Parigot_Classical_proofs_as_programs.pdf
   
   * http://www.loria.fr/~degroote/papers/tlca01.pdf
   
   * http://www.loria.fr/~degroote/papers/amsterdam01.pdf
   
   * http://www.cl.cam.ac.uk/~tgg22/publications/popl90.pdf
   
   * http://www.pps.univ-paris-diderot.fr/~ehrhard/diffusion/PLC-Griffin.pdf
