module MinimalNBC where

open import Data.Bool renaming (_∧_ to _`and`_ ; _∨_ to _`or`_)
open import Data.Fin hiding (_+_)
open import Data.Nat
open import Data.Product
open import Function using (_∘_; id)
open import Relation.Binary.PropositionalEquality

-- syntax

data Ty : Set where
  ⊥ ⊤ : Ty
  _⇒_ : Ty → Ty → Ty
infixr 7 _⇒_

data Con : ℕ → Set where
  ∙   : Con zero
  _,_ : ∀{l} → Con l → Ty → Con (suc l)
infixl 6 _,_

data _⊢_ : ∀{l} → Con l → Ty → Set where
  q    : ∀{l}{Γ : Con l}{ψ}   → Γ , ψ ⊢ ψ
  _[p] : ∀{l}{Γ : Con l}{φ ψ} → Γ ⊢ ψ → Γ , φ ⊢ ψ
  *    : ∀{l}{Γ : Con l}      → Γ ⊢ ⊤
  ⊥-e  : ∀{l}{Γ : Con l}{ψ}   → Γ ⊢ ⊥ → Γ ⊢ ψ
  Λ    : ∀{l}{Γ : Con l}{φ ψ} → Γ , φ ⊢ ψ → Γ ⊢ φ ⇒ ψ
  _$_  : ∀{l}{Γ : Con l}{φ ψ} → Γ ⊢ φ ⇒ ψ → Γ ⊢ φ → Γ ⊢ ψ
infix  5 _⊢_
infixl 8 _$_

-- semantics

⟦_⟧ : Ty → Bool
⟦ ⊥       ⟧ = false
⟦ ⊤       ⟧ = true
⟦ φ₁ ⇒ φ₂ ⟧ = (not ⟦ φ₁ ⟧) `or` ⟦ φ₂ ⟧

⟦_⟧ᶜ : ∀{l} → Con l → Bool
⟦ ∙ ⟧ᶜ = true
⟦ Γ , ψ ⟧ᶜ = ⟦ ψ ⟧ `and` ⟦ Γ ⟧ᶜ

_⊨_ : ∀{l} → Con l → Ty → Set
Γ ⊨ ψ = ⟦ Γ ⟧ᶜ ≡ true → ⟦ ψ ⟧ ≡ true

infix 5 _⊨_

-- soundness

sound : ∀{l}{Γ : Con l}{ψ} → Γ ⊢ ψ → Γ ⊨ ψ

sound (q {ψ = ψ}) _ with ⟦ ψ ⟧
sound q _  | true = refl
sound q () | false

sound (_[p] {Γ = Γ}{φ} t) _ with ⟦ Γ ⟧ᶜ | sound t
...              | true  | r = r refl
...              | false | _ with ⟦ φ ⟧
sound (t [p]) () | false | _ | true
sound (t [p]) () | false | _ | false

sound * _ = refl

sound (⊥-e t) e with sound t e
... | ()

sound (Λ {φ = φ}{ψ} t) e with ⟦ ψ ⟧ | ⟦ φ ⟧ | sound t
... | true  | true  | _ = refl
... | _     | false | _ = refl
... | false | true  | r = r e

sound (_$_ {φ = φ}{ψ} t t') e with ⟦ ψ ⟧ | ⟦ φ ⟧ | sound t | sound t'
... | true  | _     | _ | _ = refl
... | false | true  | r | _ = r e
... | false | false | _ | r = r e

module Completeness where

  -- completeness

  complete  : ∀{l}{Γ : Con l}{φ} → Γ ⊨ φ              → Γ ⊢ φ
  complete' : ∀{l}{Γ : Con l}{φ} → ⟦ Γ , φ ⟧ᶜ ≡ false → Γ , φ ⊢ ⊥

  complete  {Γ = ∙}    {⊥}     p with p refl
  ...                               | ()
  complete  {Γ = Γ , φ}{⊥}     p with ⟦ φ ⟧ `and` ⟦ Γ ⟧ᶜ  | complete' {Γ = Γ}{φ}
  complete  {Γ = Γ , φ}{⊥}     p    | true               | r with p refl
  ...                                                           | ()
  complete  {Γ = Γ , φ}{⊥}     p    | false              | r = r refl
  complete  {Γ = Γ}    {⊤}     p = *
  complete  {Γ = Γ}    {φ ⇒ ψ} p with ⟦ φ ⟧ | ⟦ ψ ⟧ | complete {Γ = Γ}{ψ} | complete' {Γ = Γ}{φ}
  complete  {Γ = Γ}    {φ ⇒ ψ} p    | true  | _     | r | _ = Λ (r p [p])
  complete  {Γ = Γ}    {φ ⇒ ψ} p    | false | true  | r | _ = Λ (r p [p])
  complete  {Γ = Γ}    {φ ⇒ ψ} p    | false | false | _ | r = Λ (⊥-e (r refl))

  complete' {Γ = Γ}    {⊥}     e = q
  complete' {Γ = ∙}    {⊤}     ()
  complete' {Γ = Γ , φ}{⊤}     e = complete' {Γ = Γ}{φ} e [p]
  complete' {Γ = Γ}    {φ ⇒ ψ} e with ⟦ φ ⟧   | ⟦ ψ ⟧ | complete {Γ = Γ}{φ} | complete' {Γ = Γ}{ψ}
  complete' {Γ = Γ}    {φ ⇒ ψ} e      | true  | _     | r | r' = (Λ (r' e)) [p] $ (q $ (r (λ _ → refl) [p]))
  complete' {Γ = Γ}    {φ ⇒ ψ} e      | false | true  | r | r' = (Λ (r' e)) [p] $ (q $ (r (trans (sym e)) [p]))
  complete' {Γ = Γ}    {φ ⇒ ψ} e      | false | false | r | r' = (Λ (r' refl)) [p] $ (q $ (r (trans (sym e)) [p]))

  -- NBC examples

  nbc : ∀{l}{Γ : Con l}{φ} → Γ ⊢ φ → Γ ⊢ φ
  nbc = complete ∘ sound

  t0 : nbc {_}{∙}{⊤} (Λ q $ *) ≡ *
  t0 = refl

  t1 : nbc {_}{∙}{⊤} ((Λ q $ Λ q) $ *) ≡ *
  t1 = refl

  t2 : nbc {_}{∙}{⊤ ⇒ ⊤} ((Λ q $ Λ q) $ (Λ q $ Λ q)) ≡ Λ (* [p])
  t2 = refl

  t3 : nbc {_}{∙}{⊤ ⇒ ⊤}(Λ q) ≡ Λ (* [p])
  t3 = refl

  t4 : nbc {_}{∙}{⊤ ⇒ ⊤}(Λ *) ≡ Λ (* [p])
  t4 = refl

  -- there is some example which didn't work nicely but I can't find
  -- it anymore

module NBC where

  -- normal forms

  data Var : ∀{l} → Con l → Ty → Set where
    zero : ∀{l}{Γ : Con l}{φ}   → Var (Γ , φ) φ
    suc  : ∀{l}{Γ : Con l}{φ ψ} → Var Γ φ → Var (Γ , ψ) φ

  data Nf  : ∀{l} → Con l → Ty → Set

  data Ne : ∀{l} → Con l → Ty → Set where
    var : ∀{l}{Γ : Con l}{φ}   → Var Γ φ → Ne Γ φ
    ⊥-e : ∀{l}{Γ : Con l}{ψ}   → Ne  Γ ⊥ → Ne Γ ψ
    _$_ : ∀{l}{Γ : Con l}{φ ψ} → Ne  Γ (φ ⇒ ψ) → Nf Γ φ → Ne Γ ψ

  data Nf where
    ne  : ∀{l}{Γ : Con l}{φ} → Ne Γ φ → Nf Γ φ
    Λ   : ∀{l}{Γ : Con l}{φ ψ} → Nf (Γ , φ) ψ → Nf Γ (φ ⇒ ψ)
    *   : ∀{l}{Γ : Con l}      → Nf Γ ⊤

  embedNf  : ∀{l}{Γ : Con l}{φ} → Nf  Γ φ → Γ ⊢ φ
  embedNe  : ∀{l}{Γ : Con l}{φ} → Ne  Γ φ → Γ ⊢ φ
  embedVar : ∀{l}{Γ : Con l}{φ} → Var Γ φ → Γ ⊢ φ

  embedNf (ne n) = embedNe n
  embedNf (Λ u)  = Λ (embedNf u)
  embedNf *      = *

  embedNe (var x) = embedVar x
  embedNe (⊥-e n) = ⊥-e (embedNe n)
  embedNe (n $ u) = embedNe n $ embedNf u

  embedVar zero = q
  embedVar (suc x) = embedVar x [p]

  norm : ∀{l}{Γ : Con l}{φ} → Γ ⊢ φ → Nf  Γ φ

  -- weakening lemmas

  _++_ : ∀{l k} → Con l → Con k → Con (k + l)
  Γ ++ ∙ = Γ
  Γ ++ (Δ , φ) = (Γ ++ Δ) , φ
  infixl 6 _++_

  left-unit+ : ∀{k} → k ≡ k + zero
  left-unit+ {zero} = refl
  left-unit+ {suc k} = cong suc (left-unit+ {k})

  cong-subst : ∀{A : Set}{P : A → Set}{a b : A}(p : a ≡ b){u : P a}{f : A → A}
               (g : ∀{b} → P b → P (f b)) → g (subst P p u) ≡ subst P (cong f p) (g u)
  cong-subst refl g = refl

  left-unit++ : ∀{k}{Γ : Con k} → subst Con left-unit+ Γ ≡ ∙ ++ Γ
  left-unit++ {zero} {∙} = refl
  left-unit++ {suc k}{Γ , φ} = subst (λ Δ → Δ ≡ ∙ ++ Γ , φ)
                                     (cong-subst {P = Con} left-unit+ {u = Γ} (λ Δ → Δ , φ))
                                     (cong (λ Δ → Δ , φ) (left-unit++ {Γ = Γ}))

  wkMidVar : ∀{k l}{Γ : Con k}{Δ : Con l}{φ ψ} → Var (Γ ++ Δ) φ → Var ((Γ , ψ) ++ Δ) φ
  wkMidVar {Γ = Γ}{∙}     x       = suc x
  wkMidVar {Γ = Γ}{Δ , φ} zero    = zero
  wkMidVar {Γ = Γ}{Δ , _} (suc x) = suc (wkMidVar {Γ = Γ}{Δ} x)

  wkMidNe : ∀{k l}{Γ : Con k}{Δ : Con l}{φ ψ} → Ne (Γ ++ Δ) φ → Ne ((Γ , ψ) ++ Δ) φ
  wkMidNf : ∀{k l}{Γ : Con k}{Δ : Con l}{φ ψ} → Nf (Γ ++ Δ) φ → Nf ((Γ , ψ) ++ Δ) φ

  wkMidNe {Γ = Γ}{∙}     (var x) = var (suc x)
  wkMidNe {Γ = Γ}{Δ , ξ} (var x) = var (wkMidVar {Γ = Γ}{Δ , ξ} x)
  wkMidNe {Γ = Γ}{Δ}     (n $ t) = wkMidNe {Γ = Γ}{Δ} n $ wkMidNf {Γ = Γ}{Δ} t
  wkMidNe {Γ = Γ}{Δ}     (⊥-e n) = ⊥-e (wkMidNe {Γ = Γ}{Δ}{⊥} n)

  wkMidNf {Γ = Γ}{Δ}        (ne n) = ne (wkMidNe {Γ = Γ}{Δ} n)
  wkMidNf {Γ = Γ}{Δ}{φ ⇒ ψ} (Λ t)  = Λ (wkMidNf {Γ = Γ}{Δ , φ} t)
  wkMidNf {Γ = Γ}{Δ}        *      = *

  wkNf : ∀{l}{Γ : Con l}{φ ψ} → Nf Γ φ → Nf (Γ , ψ) φ
  wkNf = wkMidNf {Δ = ∙}

  wkNe : ∀{l}{Γ : Con l}{φ ψ} → Ne Γ φ → Ne (Γ , ψ) φ
  wkNe = wkMidNe {Δ = ∙}

  -- substitution of normal forms

  substNf : ∀{k l}{Γ : Con k}(Δ : Con l){ψ ψ' φ}
          → Nf (Γ , ψ ++ Δ) φ → Ne (Γ , ψ') ψ → Nf (Γ , ψ' ++ Δ) φ

  substNe : ∀{k l}{Γ : Con k}(Δ : Con l){ψ ψ' φ}
          → Ne (Γ , ψ ++ Δ) φ → Ne (Γ , ψ') ψ → Ne (Γ , ψ' ++ Δ) φ
  substNe ∙       (var zero)    t = t
  substNe ∙       (var (suc x)) t = var (suc x)
  substNe (Δ , ξ) (var zero)    t = var zero
  substNe (Δ , ξ) (var (suc x)) t = wkNe (substNe Δ (var x) t)
  substNe Δ       (⊥-e n)       t = ⊥-e (substNe Δ n t)
  substNe Δ       (n $ u)       t = substNe Δ n t $ substNf Δ u t

  substNf Δ (ne n) t = ne (substNe Δ n t)
  substNf Δ (Λ  u) t = Λ (substNf (Δ , _) u t)
  substNf Δ *      t = *

  -- new completeness proof which gives normal results
  
  complete  : ∀{l}{Γ : Con l}{φ} → Γ ⊨ φ              → Nf Γ φ
  complete' : ∀{l}{Γ : Con l}{φ} → ⟦ Γ , φ ⟧ᶜ ≡ false → Ne (Γ , φ) ⊥

  complete  {Γ = ∙}    {⊥}     p with p refl
  ...                               | ()
  complete  {Γ = Γ , φ}{⊥}     p with ⟦ φ ⟧ `and` ⟦ Γ ⟧ᶜ | complete' {Γ = Γ}{φ}
  complete  {Γ = Γ , φ}{⊥}     p    | true  | r with p refl
  ...                                              | ()
  complete  {Γ = Γ , φ}{⊥}     p    | false | r = ne (r refl)
  complete  {Γ = Γ}    {⊤}     p = *
  complete  {Γ = Γ}    {φ ⇒ ψ} p with ⟦ φ ⟧ | ⟦ ψ ⟧ | complete {Γ = Γ}{ψ} | complete' {Γ = Γ}{φ}
  complete  {Γ = Γ}    {φ ⇒ ψ} p    | true  | _     | r | _ = Λ (wkNf (r p))
  complete  {Γ = Γ}    {φ ⇒ ψ} p    | false | true  | r | _ = Λ (wkNf (r p))
  complete  {Γ = Γ}    {φ ⇒ ψ} p    | false | false | _ | r = Λ (ne (⊥-e (r refl)))
  
  complete' {Γ = Γ}    {⊥}     e = var zero
  complete' {Γ = ∙}    {⊤}     ()
  complete' {Γ = Γ , φ}{⊤}     e = wkNe (complete' {Γ = Γ}{φ} e)
  complete' {Γ = Γ}    {φ ⇒ ψ} e with ⟦ φ ⟧ | ⟦ ψ ⟧ | complete {Γ = Γ}{φ} | complete' {Γ = Γ}{ψ}
  complete' {Γ = Γ}    {φ ⇒ ψ} e    | true  | _     | r | r' = substNe ∙ (r' e) (var zero $ wkNf (r (λ _ → refl))) 
  complete' {Γ = Γ}    {φ ⇒ ψ} e    | false | true  | r | r' = substNe ∙ (r' e) (var zero $ wkNf (r (trans (sym e))))
  complete' {Γ = Γ}    {φ ⇒ ψ} e    | false | false | r | r' = substNe ∙ (r' refl) (var zero $ wkNf (r (trans (sym e))))

  norm = complete ∘ sound

  lemm : ∀{l}{Γ : Con l}{φ}(t : Nf Γ φ) → norm (embedNf t) ≡ t
  lemm = {!!}
