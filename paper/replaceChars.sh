#!/bin/sh

sed -i 's/phat/$\\hat p$/g' paper.tex
sed -i 's/ᵢ/$_i$/g' paper.tex
sed -i 's/≤/$\\leq$/g' paper.tex
sed -i 's/ₙ/$_n$/g' paper.tex
sed -i 's/ρ/$\\rho$/g' paper.tex
sed -i 's/ψ/$\\psi$/g' paper.tex
sed -i 's/σ/$\\sigma$/g' paper.tex
sed -i 's/Γ/$\\Gamma$/g' paper.tex
sed -i 's/η/$\\eta$/g' paper.tex
sed -i 's/ᶜ/$\^c$/g' paper.tex
sed -i 's/φ/$\\phi$/g' paper.tex
sed -i 's/⊨/$\\vDash$/g' paper.tex
sed -i 's/◾/$\\scriptscriptstyle\\blacksquare$/g' paper.tex
sed -i 's/⇛/$\\Rrightarrow$/g' paper.tex
sed -i 's/\\ensuremath{\\neg }/\\ensuremath{\\Varid{neg}}/g' paper.tex
sed -i 's/\\ensuremath{\\neg}/\\ensuremath{\\Varid{neg}}/g' paper.tex
sed -i 's/\\neg/\\Varid{neg}/g' paper.tex
