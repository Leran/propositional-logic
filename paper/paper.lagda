%if style == newcode

\begin{code}

module paper where

\end{code}

%endif

\documentclass[runningheads,a4paper]{llncs}

\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{array}
\usepackage{bussproofs}
%\usepackage[utf8]{inputenc}
\usepackage{ucs}
\usepackage{autofe}
\usepackage{afterpage}
\usepackage{comment}
\usepackage{url}
\usepackage{times}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{stmaryrd}
\usepackage{color}
\usepackage{verbatim}
\usepackage{fancyvrb}

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {fontfamily=freemono}

\setcounter{secnumdepth}{5}

%include lhs2TeX.fmt

%include agda.fmt

%include lib.fmt

%format . = "."
%format $$ = "\!\!"

\renewcommand*\rmdefault{cmr}


\begin{document}


\mainmatter  % start of an individual contribution

% first the title is needed
\title{Formalising the Completeness Theorem of Classical Propositional
  Logic in Agda}

\titlerunning{Completeness Theorem of
  Classical Propositional Logic in Agda}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Leran Cai\and Ambrus Kaposi\and Thorsten Altenkirch}
%
\authorrunning{Leran Cai\and Ambrus Kaposi\and Thorsten Altenkirch}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{University of Nottingham\\
\verb${psylc5, psxak8, psztxa}@nottingham.ac.uk$}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

%\toctitle{Completeness Theorem}
%\tocauthor{Leran Cai, Thorsten Altenkirch, Ambrus Kaposi}
\maketitle

%The abstract should summarize the contents of the paper and should
%contain at least 70 and at most 150 words. It should be written using the
%\emph{abstract} environment.
\begin{abstract}

A computer formalisation of the completeness of the boolean model of
classical propositional logic is presented. The work follows Huth and
Ryan's proof \cite{CUP_book}. The proof is constructed for a classical
logic system in natural deduction style with all logical
connectives. The formalisation is constructive and uses the
interactive theorem prover Agda which is an implementation of
intensional Martin-L\"{o}f type theory
\cite{norell07thesis}. Functions have to be defined in a structurally
recursive way to pass the termination checker of Agda. The basic
definitions of the formal system must be carefully chosen in order to
provide a convenient environment to build correct proofs and meanwhile
prevent from getting warnings from the type checker. The formalisation
is written in an accessible way so that it can be used for educational
purposes. The full source code is available online\footnote{The
formalisation is available at
\url{http://bitbucket.org/Leran/propositional-logic}. It has been
typechecked with Agda version 2.4.2.2 and standard library 0.9.}.


\keywords{Agda, Completeness Theorem, Classical Propositional Logic}
\end{abstract}

\section{Introduction}

\subsection{Outline}

This paper describes the implementation of the completeness theorem for classical propositional logic in Agda. By interactively implementing the proof in Agda, we can attain deeper understanding of how to construct formal proofs for arbitrary propositions from their propositional atoms and how the law of the excluded middle (LEM) is used. Informally, the completeness theorem can be stated as follows:

\begin{description}
\item[(Soundness)] If a propositional formula has a proof deduced from
  the given premises, then all assignments of the premises which make
  them evaluate to true also make the formula evaluate to true.
\item[(Completeness)] If for all assignments of the axioms and
  premises which make them evaluate to true also make a propositional
  formula true, then it is always possible to construct a proof of
  this formula by applying the deduction rules on the given premises.
\end{description}

The name of the second theorem alone is completeness theorem which is confusing because the general completeness theorem usually claims them both \cite{Van_Dalen} \cite{Johnstone}. Thus we call the theorems as the soundness part and completeness part of the completeness theorem.

\subsection{Related Work}

The context of this work is formalising completeness theorems of
formal logic systems in Agda. Formalising the completeness of
intuitionistic logic with Kripke model in a proof assistant has been
finished by a number of researchers. Herbelin and Lee presented an
implementation of the completeness of Kripke model for intuitionistic
logic with implication and universal quantification in Coq
\cite{Kripke_Coq}. Blanchette et al. construct the completeness
theorem of first-order logic by using Gentzen's system and codatatypes
\cite{blanchette}. Coquand presents a fully formalised proof of the
soundness and completeness of simply-typed $\lambda$-calculus with
respect to Kripke models \cite{catarinacoquand}.

We focus on classical propositional logic in natural deduction style
and prove its soundness and completeness with regards the boolean
model. In the future, we will attempt to formalise other completeness
theorems for nonstandard Kripke model and Beth model of the
intuitionistic full first-order predicate logic (\cite{Veldman},
\cite{Friedman}).

The formalisation takes place in a constructive metalanguage of Agda,
which means that the proof has computational content. The
correspondence between classical logic and computation has been first
noticed by Griffin \cite{Griffin90aformulae-as-types}. A good summary
of the developments in this area is Urban's PhD thesis
\cite{urban2000classical}.

\section{Background}

\subsection{Agda}

All proofs were conducted in Agda (\cite{norell07thesis},
\cite{agdaWiki}), an interactive proof assistant which implements
Martin-L\"{o}f type theory \cite{HoTTbook} and can be used as a
framework to formalise formal logic systems. It is also a dependently
typed functional programming language with inductive families. $\Pi$
types (dependent functions) are written by the generalised arrow
notation |(x:A) → B| which can be interpreted as $\forall x \in
A.B$ by the Curry-Howard correspondence. Implicit arguments are
denoted by curly brackets e.g. |f : {x:A} → B|, in this case it is
not necessary to provide these arguments as Agda will try to deduce
them. We can still provide them by the |f {x = ...}|
notation. The notation |∀{x}| abbreviates |{x : _}| where
the type of |x| is inferred by Agda, the underline character
denoted a wildcard. Agda functions can be defined with infix or mixfix
notation, e.g. |_⇒_| is an infix function, |~_| is a prefix
operator. The underlines denote the positions of the arguments.

Mathematical proofs in Agda are written as structurally recursive
functions. Therefore, constructing a mathematical proof in Agda is
equivalent to constructing a well-typed function which can
terminate. Induction is performed by pattern matching on the arguments
of the function. Inductive types, and more generally, inductive
families can be defined by using the |data| keyword.

In addition, Agda supports Unicode characters, which allows us to
write proofs that look like common logic proofs on textbooks.

\subsection{Syntax and Semantics}

The formalisation of the completeness theorem requires a proper design
of the classical propositional logic system in which the syntax and
semantics should be carefully defined. For the syntax, a crucial
question is how to define propositional formulas. A propositional
formula is a string of indivisible propositional atoms and logical
connectives. In Agda, we can define them as inductive types and
thereby finding the main connective and making the tree structure of a
formula explicit is trivial.

As for semantics, every proposition in classical propositional logic
can be assigned with a boolean value. This can be implemented as a
function which takes a propositional formula as its input and its
output is the meaning of the formula.

With appropriate definitions, the soundness proof and completeness
proof can be implemented more easily. The proofs essentially follow
Huth and Ryan \cite{CUP_book} but completes the unfinished proofs
which they do not explain, especially the use of the law of the
excluded middle.

\subsection{Completeness theorem}

In this part we summarize what the completeness theorem states and
briefly introduce the ideas of their proofs. Before we formally give
the completeness theorem, we need to specify how to interpret some
fundamental concepts of the completeness theorem.

\begin{definition}
  (Sequent) A sequent is a set of formulas $\phi_1, \phi_2,...,
  \phi_n$ called premises and another formula $\psi$ which is called
  conclusion.
\end{definition}

\begin{definition}
  (Proof tree) A proof tree of a sequent $\phi_1, \phi_2,..., \phi_n
  \vdash \psi$ is a tree with root $\psi$ where the nodes are
  propositional logic deduction rules and $\phi_1, ..., \phi_n$ can be
  leaves.
\end{definition}

\begin{definition}
  (Context) A context is a set of all the premises in a sequent. The
  sequent $\phi_1, \phi_2,..., \phi_n \vdash \psi$ can be written as
  $\Gamma\ \vdash\ \psi$ where $\Gamma$ contains $\phi_i$ for all
  $1\ \leq\ i\ \leq\ n$. When the sequent only contains a theorem as
  its conclusion, the context is an empty collection $\varnothing$,
  e.g. $\varnothing \vdash\ \psi$.
\end{definition}

\begin{definition}
  (Semantic entailment) If for all valuations (mappings from the
  propositional atoms to booleans) in which the context $\Gamma$
  evaluates to true, $\psi$ also evaluates to true, we
  say that $\Gamma$ semantically entails $\psi$. It can be written as
  $\Gamma \vDash \psi$.
\end{definition}


With the interpretations above, the we can state soundness and
completeness:

\begin{theorem}
	(Soundness) Let $\phi_1, \phi_2,..., \phi_n$ and $\psi$ be propositional logic formulas. If $\phi_1, \phi_2,..., \phi_n \vdash \psi$ is valid, then $\phi_1, \phi_2,..., \phi_n \vDash \psi$ holds.
\end{theorem}

\begin{theorem}
	(Completeness) Let $\phi_1, \phi_2,..., \phi_n$ and $\psi$ be propositional logic formulas. If $\phi_1, \phi_2,..., \phi_n \vDash \psi$ holds, then $\phi_1, \phi_2,..., \phi_n \vdash \psi$ is valid.
\end{theorem}

The completeness theorem consists of both. Therefore, the corollary
can be stated as follows:

\begin{corollary}
	(Completeness Theorem) Let $\phi_1, \phi_2,..., \phi_n, \psi$ be formulas of propositional logic. $\phi_1, \phi_2,..., \phi_n \vDash \psi$ holds iff the sequent $\phi_1, \phi_2,..., \phi_n \vdash \psi$ is valid.
\end{corollary}



\subsubsection{Soundness proof}

The soundness part asserts: $\Gamma \vdash \psi \rightarrow \Gamma
\vDash \psi$. To prove it, we do induction on the depth of the proof
tree of $\Gamma \vdash \psi$. In essence, this induction checks the
\emph{last propositional deduction rule} the proof of $\Gamma \vdash
\psi$ uses. For example, if the least significant connective in $\psi$
is $\land$ which means $\psi$ has form $\phi_1 \land \phi_2$, then we
can say that the last rule in the proof of $\Gamma \vdash \psi$ is
conjunction ($\land$) introduction rule which is the only way to
construct propositions which have the form $\phi_1 \land \phi_2$. Then
we use the induction hypothesis to prove that $\Gamma \vDash \phi_1$
and $\Gamma \vDash \phi_2$ both hold and use them to prove $\Gamma
\vDash \phi_1 \land \phi_2$ ($\Gamma \vDash \psi$) by discussing how
logic connectives handle truth values. In Agda, the induction on the
depth of the proof tree becomes structural induction defined by
pattern matching.

\subsubsection{Completeness proof}

The completeness part asserts: $\Gamma \vDash \psi \rightarrow \Gamma
\vdash \psi$ where $\Gamma$ consists of $\phi_1,
\phi_2,...,\phi_n$. This theorem should be proved in three steps:

\begin{description}
	\item [Lemma 1.] Our first step is to prove the theorem: $\phi_1, \phi_2,...,\phi_n \vDash \psi \rightarrow \varnothing \vDash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$. This states that if $\phi_1, \phi_2,...,\phi_n$ semantically entails $\psi$ then $\phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$ is a \emph{tautology}.
	\item [Lemma 2.] In this step, the \emph{weak version} of completeness which is $\varnothing \vDash \eta \rightarrow \varnothing \vdash \eta$ is proved. This asserts that \emph{all tautologies have a proof}. By proving this, we can demonstrate that $\varnothing \vDash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi \rightarrow \varnothing \vdash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$.
	\item [Lemma 3.] The last step is: $\varnothing \vdash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi \rightarrow \phi_1, \phi_2,...,\phi_n \vdash \psi$. Given $\varnothing \vdash \phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$, we add in $\phi_1, \phi_2,...,\phi_n$ as premises and we use implication ($\Rightarrow$) elimination rule to reduce the proposition from $\phi_1 \Rightarrow \phi_2 \Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$ to $\psi$. In the end we can obtain $\phi_1, \phi_2,...,\phi_n \vdash \psi$.
\end{description}

By combining these three proofs together we can prove the completeness.

\section{Implementation in Agda}

In this section, we first follow the definitions in the previous
section to implement the syntax and semantics for our formal
system. Then the detailed discussion of the proof of completeness
theorem will be given as well as the code implementation in Agda.

We use the following notational conventions:
\begin{alignat*}{3}
  & \Gamma && \text{contexts} \\
  & \phi_i, \psi, \chi, \eta \hspace{3em} && \text{propositions} \\
  & p_i && \text{propositional atoms} \\
  & \rho && \text{valuation} \\
  & \sigma, \sigma_i, \delta && \text{proof trees}
\end{alignat*}


\subsection{Syntax}

The basic object in classical propositional logic is the well-formed
propositional formula. We define the inductive type |Props| whose
inhabitants are well-formed propositions. Using the constructors |⊥|,
|⊤|, |patom|, $\dots$ is the only way to construct an object which has
type |Props|.

\begin{code}
data Props : Set where
  ⊥ ⊤           : Props
  patom         : Fin n   → Props
  ~_            : Props   → Props
  _∨_ _∧_ _⇒_   : Props   → Props → Props
\end{code}
The number of propositional atoms is denoted |n|, this is a natural
number parameter of the whole formalisation. Thus, a formula can refer
to |n| different propositional atoms, this is expressed by the |patom|
constructor which takes a value of type |Fin n|. For a natural number
|n| the datatype |Fin n| is the type of natural numbers less than
|n|.

For example, the formula $p_0 \Rightarrow (p_0 \vee \sim p_1)$ which
mentions on 2 propositional atoms is represented by |patom zero ⇒
(patom zero ∨ ~ patom (suc zero))|. |patom zero| is the first
propositional atom.

A \emph{context} is defined as a list where one can insert new
elements at the end. In Agda, we call it |Cxt|. It has one natural
number index, this carries the length of the context.

\begin{code}
data Cxt : ℕ → Set where
  ∅    : Cxt zero
  _∙_  : {l : ℕ} → Cxt l → Props → Cxt (suc l)
\end{code}

Then we can use |Cxt| and |Props| to define the type of proof
trees. An element of the data type |Γ ⊢ φ| will be a proof tree
which has |φ| at its root and the assumptions in |Γ| at its
leaves. The constructors represent the deduction rules. In this way,
we can guarantee that every proof tree in Agda is valid since they can
only be constructed by using deduction rules.

We write the types of the constructors in a deduction rule style, the
horizontal lines are just comments in Agda.

\begin{code}
data _⊢_ : {l : ℕ}(Γ : Cxt l)(ψ : Props) → Set where

  var    : ∀{l}{Γ : Cxt l}{ψ}       → Γ ∙ ψ ⊢ ψ

  weaken : ∀{l}{Γ : Cxt l}{φ ψ}     → Γ ⊢ ψ
                                    -- --------------
                                    → Γ ∙ φ ⊢ ψ

  ⊤-i    : ∀{l}{Γ : Cxt l}          → Γ ⊢ ⊤

  ⊥-e    : ∀{l}{Γ : Cxt l}{ψ}       → Γ ⊢ ⊥
                                    -- --------
                                    → Γ ⊢ ψ

  ~-i    : ∀{l}{Γ : Cxt l}{ψ}       → Γ ∙ ψ ⊢ ⊥
                                    -- --------------
                                    → Γ ⊢ ~ ψ

  ~-e    : ∀{l}{Γ : Cxt l}{ψ}       → Γ ⊢ ψ → Γ ⊢ ~ ψ
                                    -- --------------------------
                                    → Γ ⊢ ⊥

  ⇒-i    : ∀{l}{Γ : Cxt l}{φ ψ}     → Γ ∙ φ ⊢ ψ
                                    -- --------------
                                    → Γ ⊢ φ ⇒ ψ
                                
  ⇒-e    : ∀{l}{Γ : Cxt l}{φ ψ}     → Γ ⊢ φ ⇒ ψ → Γ ⊢ φ
                                    -- -----------------------------
                                    → Γ ⊢ ψ

  ∧-i    : ∀{l}{Γ : Cxt l}{φ ψ}     → Γ ⊢ φ → Γ ⊢ ψ
                                    -- -----------------------
                                    → Γ ⊢ φ ∧ ψ
                                    
  ∧-e₁   : ∀{l}{Γ : Cxt l}{φ ψ}     → Γ ⊢ φ ∧ ψ
                                    -- --------------
                                    → Γ ⊢ φ
                                    
  ∧-e₂   : ∀{l}{Γ : Cxt l}{φ ψ}     → Γ ⊢ φ ∧ ψ
                                    -- --------------
                                    → Γ ⊢ ψ

  ∨-i₁   : ∀{l}{Γ : Cxt l}{φ ψ}     → Γ ⊢ φ
                                    -- --------------
                                    → Γ ⊢ φ ∨ ψ
                                    
  ∨-i₂   : ∀{l}{Γ : Cxt l}{φ ψ}     → Γ ⊢ ψ
                                    -- --------------
                                    → Γ ⊢ φ ∨ ψ
                                    
  ∨-e    : ∀{l}{Γ : Cxt l}{φ ψ χ}   → Γ ⊢ φ ∨ ψ 
                                    → Γ ∙ φ ⊢ χ 
                                    → Γ ∙ ψ ⊢ χ
                                    -- --------------
                                    → Γ ⊢ χ
  
  lem    : ∀{l}{Γ : Cxt l}{ψ}       → Γ ⊢ ψ ∨ ~ ψ
\end{code}

We handle variable binding by De Bruijn indices implemented by the
variable rule |var| and the weakening rule |weaken|. The
variable rule says that if the context ends with |ψ|, then we can
prove |ψ|. The weakening rule asserts that if |ψ| can be deduced
from the one context, then we can also deduce it from an extended
context.

An example of a derivation tree is
\begin{prooftree}
  \AxiomC{}
  \RightLabel{var}
  \UnaryInfC{$\varnothing\cdot \phi \land \psi \vdash \phi \land \psi$}
  \RightLabel{$\land$-e$_2$}
  \UnaryInfC{$\varnothing\cdot \phi \land \psi \vdash \psi$}
  \AxiomC{}
  \RightLabel{var}
  \UnaryInfC{$\varnothing\cdot \phi \land \psi \vdash \phi \land \psi$}
  \RightLabel{$\land$-e$_1$}
  \UnaryInfC{$\varnothing\cdot \phi \land \psi \vdash \phi$}
  \RightLabel{$\land$-i}
  \BinaryInfC{$\varnothing\cdot \phi \land \psi \vdash \psi \land \phi$}
  \RightLabel{$\Rightarrow$-i}
  \UnaryInfC{$\varnothing\vdash \phi \land \psi \Rightarrow \psi \land \phi$}
\end{prooftree}
it is represented by the Agda term
\begin{spec}
⇒-i (∧-i (∧-e₂ var) (∧-e₁ var)).
\end{spec}

\subsection{Semantics}

In Agda, we use \emph{semantic brackets} |⟦_⟧| to give propositional
formulas meanings. It is a function which takes a formula as an input
and returns a boolean value as its semantics.

Any proposition either is a propositional atom or consists of a
certain number of propositions and propositional connectives. Thus the
semantics for an arbitrary proposition should depend on the valuation
of each propositional atom and the behaviours of each propositional
connective. Valuations (elements of the type |Val|) are functions from
the |n|-element set to booleans.

\begin{code}
Val = Fin n → Bool

⟦_⟧ : Props → Val → Bool
⟦ ⊥         ⟧ ρ = false
⟦ ⊤         ⟧ ρ = true
⟦ patom x   ⟧ ρ = ρ x
⟦ ~ φ       ⟧ ρ = not (⟦ φ ⟧ ρ)
⟦ φ₁ ∨ φ₂   ⟧ ρ = ⟦ φ₁ ⟧ ρ `or`  ⟦ φ₂ ⟧ ρ
⟦ φ₁ ∧ φ₂   ⟧ ρ = ⟦ φ₁ ⟧ ρ `and` ⟦ φ₂ ⟧ ρ
⟦ φ₁ ⇒ φ₂   ⟧ ρ = not (⟦ φ₁ ⟧ ρ) `or` ⟦ φ₂ ⟧ ρ 
\end{code}

In this definition, |not|, |and|, |or| are the usual boolean
operations.

In essence, this way of defining the semantics of propositions is the
same as doing induction on syntax trees of propositions. For example,
in |φ₁ ∨ φ₂| we investigate the meaning of left subtree and the
meaning of the right subtree. Then we use the meaning of the |∨| on
the node to define the meaning of the whole tree. On the leaves of
proposition syntax trees are all propositional atoms which are not
divisible. Their semantics are determined by the valuation |ρ| we
give.

The meaning of a context is just the conjunction of the meanings of its
formulas.

\begin{code}
⟦_⟧ᶜ : {l : ℕ} → Cxt l → Val → Bool
⟦ ∅     ⟧ᶜ ρ = true
⟦ Γ ∙ φ ⟧ᶜ ρ = ⟦ Γ ⟧ᶜ ρ `and` ⟦ φ ⟧ ρ
\end{code}

Now we have defined both |Props| and |Cxt|. The semantic entailment is
given as follows:

\begin{code}
_⊨_ : {l : ℕ} → Cxt l → Props → Set
Γ ⊨ ψ = ∀ ρ → ⟦ Γ ⟧ᶜ ρ ≡ true → ⟦ ψ ⟧ ρ ≡ true
\end{code}

This |_⊨_| relation states that for all valuations, if all formulas in
the context evaluate to true then the conclusion also evaluates to
true. |_≡_| denotes the internal equality of Agda.

\subsection{Soundness proof}

We implement the soundness theorem |Γ ⊢ ψ → Γ ⊨ ψ| as the following
function:
\begin{code}
soundness : ∀ {l}{Γ : Cxt l}{ψ : Props} → Γ ⊢ ψ → Γ ⊨ ψ
\end{code}
If we expand the definition of ⊨, we get the following type:
\begin{code}
soundness : ∀ {l}{Γ : Cxt l}{ψ : Props} → Γ ⊢ ψ
              → (∀ ρ → ⟦ Γ ⟧ᶜ ρ ≡ true → ⟦ ψ ⟧ ρ ≡ true)
\end{code}

As we mentioned before, we prove this by structural induction on the
proof tree. By pattern matching on the proof tree |σ| which has type
|Γ ⊢ ψ|, we can check which is the last deduction rule. In Agda's interactive
interface,
\begin{code}
soundness σ = {!!}
\end{code}
becomes
\begin{code}
soundness var = {!!}
soundness (weaken σ) = {!!}
soundness ⊤-i = {!!}
soundness (⊥-e σ) = {!!}
soundness (~-i σ) = {!!}
soundness (~-e σ σ₁) = {!!}
soundness (⇒-i σ) = {!!}
soundness (⇒-e σ σ₁) = {!!}
soundness (∧-i σ σ₁) = {!!}
soundness (∧-e₁ σ) = {!!}
soundness (∧-e₂ σ) = {!!}
soundness (∨-i₁ σ) = {!!}
soundness (∨-i₂ σ) = {!!}
soundness (∨-e σ σ₁ σ₂) = {!!}
soundness lem = {!!}
\end{code}
after pattern matching. The |{!!}| parts denote holes in the
proof which need to be filled in by the user.

We exemplify filling one of the holes, the one for conjunction
introduction rule. The fundamental idea is to use induction hypothesis
to prove the semantic entailment relation between the context and the
component of the original proposition and discuss how the logic
connectives handle truth values. One important idea is that using
induction hypothesis means recursively calling the \emph{soundness}
function. The deduction rule of conjunction introduction is the
following:
\begin{prooftree}
	\AxiomC{$\Gamma \vdash \phi_1$}
	\AxiomC{$\Gamma \vdash \phi_2$}
	\RightLabel{$\land$-i}
	\BinaryInfC{$\Gamma \vdash \phi_1 \land \phi_2$}
\end{prooftree}
In the implementation, the left subtree (proving |Γ ⊢ φ₁|) is denoted
|σ₁|, the right subtree (proving |Γ ⊢ φ₂|) is denoted |σ₂|. 
\begin{code}
soundness {ψ = φ₁ ∧ φ₂} (∧-i σ₁ σ₂) ρ ⟦Γ⟧≡true
  with      ⟦ φ₁ ⟧ ρ  | inspect ⟦ φ₁ ⟧ ρ   | ⟦ φ₂ ⟧ ρ  | inspect ⟦ φ₂ ⟧ ρ
...      |  true      | [ _ ]              | true      | [ _ ]          = refl
...      |  true      | [ _ ]              | false     | [ ⟦φ₂⟧≡false ]

                       = ⟦φ₂⟧≡false ⁻¹ ◾ (soundness σ₂ ρ ⟦Γ⟧≡true)
                     
...      |  false     | [ ⟦φ₁⟧≡false ]     | _         | [ _ ]

                       = ⟦φ₁⟧≡false ⁻¹ ◾ (soundness σ₁ ρ ⟦Γ⟧≡true)

\end{code}

Our goal is to prove soundness when the proposition has form |∧-i σ₁
σ₂ : Γ ⊢ φ₁ ∧ φ₂|. The result needs to be of type |Γ ⊨ φ₁ ∧ φ₂|, that
is
\begin{code}
∀ ρ → ⟦ Γ ⟧ᶜ ρ ≡ true → ⟦ φ₁ ∧ φ₂ ⟧ ρ ≡ true.
\end{code}
The valuation is denoted by |ρ| and the witness that the context
evaluates to |true| is denoted |⟦Γ⟧=true|. We prove soundness by
inspecting the meaning of |φ₁| and |φ₂|, that is pattern matching on
the values of |⟦ φ₁ ⟧ ρ| and |⟦ φ₂ ⟧ ρ|, respectively, by using the
|with| construct of Agda. |with| is like a dependent |case|
analysis. |inspect| provides witnesses of patterns matching,
e.g. |inspect ⟦ φ₂ ⟧ ρ| proves |⟦ φ₂ ⟧ ρ ≡ false| in the case when |⟦
φ₂ ⟧ ρ| was matched by |false|.

In the case when both of meanings are |true|, the return type will
just reduce to |true ≡ true| which can be proven by |refl|
(reflexivity, the Agda constructor for the |_≡_| type).

In the cases when one of them is false, we use the induction
hypothesis. For example, if |⟦ φ₁ ⟧ ρ| is |true| and |⟦ φ₂ ⟧ ρ| is
|false|, we need to prove |false ≡ true|. This can be done by using
transitivity (|_◾_|) to compose the equalities
\begin{code}
φ₂≡false ⁻¹ : false ≡ ⟦ φ₂ ⟧ ρ
\end{code}
and
\begin{code}
soundness σ₂ ρ ⟦Γ⟧≡true : ⟦ φ₂ ⟧ ρ ≡ true.
\end{code}

The rest of the deduction rules of classical propositional logic can
be proved in a similar way.

\subsection{Completeness proof}

In this section we present the proof of completeness and its
implementation in Agda. The general theorem is $\Gamma \vDash \psi
\rightarrow \Gamma \vdash \psi$. As mentioned before, the general
theorem is split into three lemmas.

\begin{code}
lemma1   : ∀{l}{Γ : Cxt l}{ψ : Props} → Γ ⊨ ψ → ∅ ⊨ Γ ⇛ ψ
lemma2   : ∀{η : Props}               → ∅ ⊨ η → ∅ ⊢ η
lemma3   : ∀{l}{Γ : Cxt l}{ψ : Props} → ∅ ⊢ (Γ ⇛ ψ) → Γ ⊢ ψ

completeness : ∀{l}{Γ : Cxt l}{φ : Props} → Γ ⊨ φ → Γ ⊢ φ
completeness = lemma3 ∘ lemma2 ∘ lemma1
\end{code}

\subsubsection{Proof of Lemma 1.} |Γ ⊨ ψ → ∅ ⊨ Γ ⇛ ψ| \\

The idea is to move every propositional formula in the context to the
right hand side of the turnstile and combine them with the given
proposition $\psi$ to construct a tautology. For example, if |Γ|
contains n propositions which are $\phi_1, \phi_2,..., \phi_n$, the
proposition we will construct is $\phi_1 \Rightarrow \phi_2
\Rightarrow...\Rightarrow\phi_n \Rightarrow \psi$. The reason why
$\Gamma \Rrightarrow \psi$ is a tautology is simple: the only
situation where this evaluates to false is when each proposition in
$\Gamma$ evaluates to true and $\psi$ evaluates to false. However,
this is impossible because we know $\Gamma \vDash \psi$ which means
$\psi$ must evaluate to true when everything in $\Gamma$ evaluates to
true.

We first implement a function |Γ ⇛ ψ| by induction on |Γ|:

\begin{code}
_⇛_ : ∀{l}(Γ : Cxt l)(ψ : Props) → Props
∅       ⇛ ψ     = ψ
(Γ ∙ φ) ⇛ ψ     = Γ ⇛ φ ⇒ ψ
\end{code}

Then we prove the proposition |Γ ⇛ φ| we constructed is a tautology:

\begin{code}
lemma1       : ∀{l}{Γ : Cxt l}{ψ : Props} → Γ ⊨ ψ → ∅ ⊨ Γ ⇛ ψ
lemma1 {Γ = ∅}      {ψ}  ∅⊨ψ  ρ  ⟦∅⟧≡true = ∅⊨ψ ρ ⟦∅⟧≡true
lemma1 {Γ = Γ ∙ φ}  {ψ}  ∅⊨ψ  ρ  ⟦∅⟧≡true
  = lemma1 {Γ = Γ} (λ ρ' → oneStep ρ' (∅⊨ψ ρ')) ρ refl
\end{code}
The idea of the proof is that we investigate the form of $\Gamma$ by
pattern matching in Agda. The case when $\Gamma$ is $\varnothing$ is
trivial. If $\Gamma$ has form |(Γ ∙ φ)|, then we use the helper
function
\begin{code}
oneStep : ∀ ρ → (⟦ Γ ∙ φ ⟧ᶜ ρ ≡ true → ⟦ ψ ⟧ ρ ≡ true) 
              → (⟦ Γ ⟧ᶜ ρ ≡ true → ⟦ φ ⇒ ψ ⟧ ρ ≡ true)
\end{code}
to prove that by first pattern matching on the truth value of φ and
ψ. This enables us to move one proposition from the left hand side to
the right hand side of $\vDash$. By repeating this step, we can move
all propositions to the right hand side. |lemma1| is calling |oneStep|
as many times as required. In the end, we will get a semantical
entailment relation: |∅ ⊨ φ₁ ⇒ φ₂ ⇒ ... ⇒ φₙ ⇒ ψ| which is |∅ ⊨ Γ ⇛
ψ|.

\subsubsection{Proof of Lemma 2.} |∅ ⊨ η → ∅ ⊢ η| \\

This lemma (weak completeness) is the most difficult part of the proof
because |η| can be an arbitrary proposition and we have to present a
concrete proof that we can always construct a proof by using deduction
rules regardless of the inner structure of the tautology |η|.

Given a tautology |η| which contains n propositional atoms
$p_0,p_1,...p_{n-1}$, there are $2^n$ different lines of valuations in
the truth table of |η| and |η| always evaluates to true no matter what
valuation it uses. The key idea of proving weak completeness is that
we encode each line of valuation in the truth table as a specific
context. Then we first give an intermediate proof which says that from
any one of these $2^n$ different contexts we can always derive
|η|. Then we embed this proof in the proof of weak completeness by
proving that from an empty context (∅) we can construct all of these
$2^n$ contexts by using the law of excluded middle.

To encode the $2^n$ lines of valuation, we introduce a definition
|Cxt[ ρ ]| which explains how to translate a truth assignment into a
context.

\begin{definition}
(Cxt[|ρ|]): Given a valuation $\rho$ of $n$ propositional atoms named
  $p_0,p_1,...,p_{n-1}$, we construct a context based on them. $Cxt[\rho]$
  is a context which contains n propositions: $\hat p_0, \hat
  p_1,...,\hat p_{n-1}$ where $\hat p_i = p_i$ if
  $p_i$ is assigned true by ρ, otherwise $\hat p_i = \sim p_i$.
\end{definition}

The two steps in the proof are as follows:

\begin{code}
lemma21 : ∀{η : Props} → ∅ ⊨ η → (∀ ρ → Cxt[ ρ ] ≤-refl ⊢ η)
lemma22 : ∀{η : Props} → (∀ ρ → Cxt[ ρ ] ≤-refl ⊢ η) → ∅ ⊢ η

lemma2 = lemma22 ∘ lemma21
\end{code}

\paragraph{\textbf{Formalising Cxt[$\rho$] in Agda\\\\}}

Before formalising \emph{Cxt[$\rho$]}, we first define $\hat p[\rho]$
to construct $\hat p_i$ given a valuation ρ and a number |x| which has
type |Fin n| in Agda in preparation for defining $Cxt[\rho]$.

\begin{code}
phatᵢ[ρ] : ∀ ρ → Fin n → Props
phatᵢ[ρ] ρ x with ρ x
phatᵢ[ρ] ρ x | true  =   patom x
phatᵢ[ρ] ρ x | false = ~ patom x
\end{code}

$Cxt[\rho]$ consists of $\hat p_i$s, but we cannot define it in Agda
by doing pattern matching on the number of propositional variables |n|
because it is a parameter of the whole proof. Instead we define
cut-off contexts: |Cxt[ ρ ]( α )| will be the first |a| elements of
$Cxt[\rho]$ where |α : a ≤ n|. This way we can define |Cxt[ ρ ]( α )|
by induction on |a|. We also need these cut-off contexts to prove
lemmas from Lemma 2.2.2 on.

\begin{code}
Cxt[_] : ∀ ρ {a} → a ≤ n → Cxt l
Cxt[_] _ {zero}   z≤n  = ∅
Cxt[_] ρ {suc a}  α    = Cxt[ ρ ] (≤⇒pred≤ _ _ α) ∙ phatᵢ[ ρ ] (fromℕ≤ α)
\end{code}

Another way to represent |a| and |a ≤ n| would be to use an |a : Fin
(suc n)|. However, this would add additional difficulties of
converting elements of |Fin n| into |ℕ| and vice versa.

\paragraph{\textbf{Proof of lemma 2.1 \\\\}}

Lemma 2.1 says that given |∅ ⊨ η|, for all valuations |ρ|, |Cxt[ρ] ⊢
η|. We prove it by proving the following two lemmas mutually, and then
using lemma 2.1.1.

\begin{code}
lemma211 : ∀{ψ} ρ → (⟦ ψ ⟧ ρ ≡ true)  → (Cxt[ ρ ] ≤-refl) ⊢ ψ
lemma212 : ∀{ψ} ρ → (⟦ ψ ⟧ ρ ≡ false) → (Cxt[ ρ ] ≤-refl) ⊢ ~ ψ

lemma21 σ ρ = lemma211 ρ (σ ρ refl)
\end{code}

|≤-refl| is the proof that the relation |_≤_| is reflexive.

We illustrate the proof of |Cxt[ρ]⊢ψ| by the case when |ψ| has form
|φ₁ ∧ φ₂| as an example. The other cases are similar.
\begin{code}
lemma211 {ψ = φ₁ ∧ φ₂} ρ _  
  with  ⟦ φ₁ ⟧ ρ  | inspect ⟦ φ₁ ⟧ ρ  | ⟦ φ₂ ⟧ ρ  | inspect ⟦ φ₂ ⟧ ρ
  
lemma211 {ψ = φ₁ ∧ φ₂} ρ _    |  true      | [ φ₁≡true ]       | true      | [ φ₂≡true ]
                             
          = ∧-i (lemma211 ρ φ₁≡true) (lemma211 ρ φ₂≡true)
                             
lemma211 {ψ = φ₁ ∧ φ₂} ρ ()   |  true      | [ _ ]             | false     | [ _ ]
lemma211 {ψ = φ₁ ∧ φ₂} ρ ()   |  false     | [ _ ]             | _         | [ _ ]
\end{code}

We inspect the meaning of |φ₁| and |φ₂|, and if both are true, we use
the induction hypotheses. If one of them is false, the type of the
third argument of the function which was originally |⟦ ψ ⟧ ρ ≡ true|
will reduce to |false ≡ true| and this can be pattern matched with the
absurd pattern |()| of Agda. If there is an absurd pattern on the left
hand side, we don't need a right hand side.

\paragraph{\textbf{Proof of lemma 2.2} \\\\}

Lemma 2.2 says that if for all valuations $\rho$, $Cxt[\rho]
\vdash\eta$ is valid, then we can deduce $\eta$ from only the basic
deduction rules without the help of any other premises. In other
words, $\eta$ is a theorem.

\begin{code}
lemma22 : ∀{η : Props} → (∀ ρ → Cxt[ ρ ] ≤-refl ⊢ η) → ∅ ⊢ η
\end{code}

The idea of the proof is straightforward. We start with a context
containing $n$ propositions if $\eta$ has at most $n$ propositional
atoms. In the end, the context contains zero propositions. It is
natural to think of reducing the number of propositions in the context
one at a time, by using the law of excluded middle.

To prove Lemma 2.2, we first prove a more general lemma. As we
cannot do induction on |n|, we need to introduce introduce a new
parameter |a| on which we do induction, and |b| will remain unchanged
during computation.

\begin{code}
lemma221   : ∀{η}{a b}{an : a ≤ n}{bn : b ≤ n}(ba : b ≤ a)
           → (∀ ρ → Cxt[ ρ ] an ⊢ η) → (∀ ρ → Cxt[ ρ ] bn ⊢ η)
\end{code}

One special case of Lemma 2.2.1 is Lemma 2.2 where |a = n| and |b = 0|
and we add a dummy valuation:
\begin{code}
lemma22 σ = lemma221 {bn = z≤n} z≤n σ (λ _ → true).
\end{code}

Lemma 2.2.2 will reduce the length of the context by one:
\begin{code}
lemma222   : ∀{a}{α : a < n}{η : Props}
           → (∀ ρ → Cxt[ ρ ] {suc a} α ⊢ η)
           → (∀ ρ → Cxt[ ρ ] {a} (≤⇒pred≤ _ _ α) ⊢ η).
\end{code}
This is the step that is repeated by lemma 2.2.1 until we reach |b|:
\begin{code}
lemma221 {a = a}{b} ba σ ρ with a ≟ b
lemma221 ba σ ρ | yes refl

  = subst (λ z → Cxt[ ρ ] z ⊢ _) (≤-unique _ _) (σ ρ)

lemma221 {a = zero} z≤n  σ ρ | no ¬p

  = subst (λ x → Cxt[ ρ ] x ⊢ _) (≤-unique _ _) (σ ρ)

lemma221 {a = suc a} ba  σ ρ | no ¬p

  = lemma221 (lemma-≤2 ba (¬p ∘ _⁻¹)) (lemma222 σ) ρ
\end{code}
|subst| replaces equal elements in a type (|subst : (P : A → Set) → a
≡ b → P a → P b|), |≤-unique| says that proofs of the ordering
relation are unique, |lemma-≤2| is a helper lemma regarding orderings.

The proof of lemma 2.2.2 is using the excluded middle:
\begin{code}
lemma222 {a = a} {α} {η} σ ρ

  = ∨-e (lem {ψ = patom (fromℕ≤ α)})
        (subst (λ z → z ⊢ η) (lemma-Cxt ρ α) (σ (ρ [ α ↦ true ])))
        (subst (λ z → z ⊢ η) (lemma-Cxt ρ α) (σ (ρ [ α ↦ false ])))
\end{code}

The proof tree is the following:

\begin{prooftree}
    \AxiomC{}
    \RightLabel{lem}
	\UnaryInfC{$\scriptstyle \hat p_0,\hat p_1,...,\hat p_{a-1} \vdash p_a \lor \sim p_a$}
	\AxiomC{$\scriptstyle \hat p_0,\hat p_1,...,\hat p_{a-1}, p_a \vdash \eta$}
	\AxiomC{$\scriptstyle \hat p_0,\hat p_1,...,\hat p_{a-1},\sim p_a \vdash \eta$}
	\RightLabel{$\lor$-e}
	\TrinaryInfC{$\scriptstyle \hat p_0,\hat p_1,...,\hat p_{a-1} \vdash \eta$}
\end{prooftree}

Obviously the first premise can be deduced by using LEM. The key is to
get the second and the third premises by induction. First we have a
powerful condition in our hands (look at the type of |lemma222|): |∀
ρ, Cxt[ρ](suc a) ⊢ η|. For a fixed $\rho$, we know that |Cxt[ρ](a)| is
$\hat p_0,\hat p_1,...,\hat p_{a-1}$. We then construct two specific
valuations |ρ[ a ↦ true ]| and |ρ[ a ↦ false ]|. In the former, the
first |a| assignments are equal to the first |a| assignments in
|ρ|, and the (|1 + a|)th element is |true|. Now we can see that
|Cxt[ ρ [ a ↦ true ] ] (suc a)| is $\hat p_0,\hat p_1,...,\hat
p_{a-1}, p_a$. As we mentioned above, we have a powerful premise in
our hands: |∀ ρ → Cxt[ ρ ] (suc a) ⊢ η|. We replace the general |ρ| by
our |ρ[ a ↦ true ]|. From this we get $\hat p_0,\hat p_1,...,\hat
p_{a-1}, p_a \vdash \eta$ which is the second premise in the proof
tree. Similarly, we construct another valuation |ρ [ a ↦ false ]| and
get a proof for the third premise in the proof tree above.

|lemma-Cxt| is a helper lemma stating that |Cxt[ ρ [ a ↦ true ] ] (1 +
 a) | is the same as the composition of the contexts |Cxt[ ρ ] a| and
 |patom a| (with some additional noise given by proofs that |a| is
 less than |n|).

\subsubsection{Lemma 3} |∅ ⊢ (Γ ⇛ ψ) → Γ ⊢ ψ| \\

This is the last thing we need to prove. The expanded view of $\Gamma\
\Rrightarrow\ \psi$ is $\phi_1 \Rightarrow \phi_2
\Rightarrow...\Rightarrow \phi_n \Rightarrow \psi$. We present the
proof for how to move one $\phi_i$ from the right hand side to the
left hand side. This can be used recursively to move all $\phi$s to
the left.

\begin{prooftree}
	\AxiomC{$\phi_1,\phi_2,...,\phi_n\vdash\phi_1\Rightarrow\phi_2\Rightarrow...\Rightarrow\phi_n\Rightarrow\psi$}
	\AxiomC{$\phi_1\in\phi_1,\phi_2,...,\phi_n$}
	\RightLabel{var, weaken}
	\UnaryInfC{$\phi_1,\phi_2,...,\phi_n\vdash\phi_1$}
	\RightLabel{$\Rightarrow$-e}
	\BinaryInfC{$\phi_1,\phi_2,...\phi_n\vdash\phi_2\Rightarrow\phi_3\Rightarrow...\Rightarrow\phi_n\Rightarrow\psi$}
\end{prooftree}

In Agda, its formalisation is given as follows:

\begin{code}
lemma3 : ∀{l}{Γ : Cxt l}{ψ : Props} → ∅ ⊢ (Γ ⇛ ψ) → Γ ⊢ ψ
lemma3 {Γ = ∅}     {ψ} σ = σ
lemma3 {Γ = Γ ∙ φ} {ψ} σ

  = ⇒-e (weaken (lemma3 {Γ = Γ} σ)) var
\end{code}

\section{Conclusion}

This paper implements in Agda a formalisation of the proof for
soundness and completeness of classical propositional logic. In order
to construct the proof with Agda recognisable functions, we have to
find some more general propositions to prove so that the functions we
need to construct in Agda are structurally recursive and can pass the
termination checker. The original proof then will become a special
case of the more general propositions that we give. This intention in
return provides us with a higher level view of what the proof actually
says. In the end, we achieve a rigorous and clear proof of
completeness theorem in classical propositional logic.

An interesting further step would be to investigate the computational
content of this proof in the style of normalisation by completeness
(\cite{berger91nbe}, \cite{alti:ctcs95}). The completeness and the
soundness proofs can be composed which gives a normalisation procedure
on proof trees (which can be thought about as computer programs).

\begin{code}
nbc : ∀{l}{Γ : Cxt l}{φ : Props} → Γ ⊢ φ → Γ ⊢ φ
nbc = completeness ∘ soundness
\end{code}






\bibliography{ref}{}
\addcontentsline{toc}{section}{References}
\bibliographystyle{splncs03}


\end{document}
